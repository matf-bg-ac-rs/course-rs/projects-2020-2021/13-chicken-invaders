#ifndef METEOR_H
#define METEOR_H

#include <QGraphicsPixmapItem>
#include <QGraphicsRectItem>
#include <QGraphicsScene>
#include <QLabel>
#include <QList>
#include <QMediaPlayer>
#include <QMovie>
#include <QObject>
#include <QTimer>
#include <algorithm>
#include <stdlib.h>

class Meteor : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    Meteor(int x, int y);
    void destroy();
    void haveToDie();
    void dieSound();

    int movement_type;
    int hp;
    int meteor_type;
    int speed_movement;
    double move_x;
    double move_y;

protected:
    void drop_gifts();
    void advance(int a) override;

private:
    QPixmap Meteor_3 = QPixmap(":/images/meteor");
    QPixmap Meteor_2 = QPixmap(":/images/bite_1");
    QPixmap Meteor_1 = QPixmap(":/images/bite_2");

    void move_fall();

public slots:
    void decreaseHp(int x);
};

#endif // METEOR_H
