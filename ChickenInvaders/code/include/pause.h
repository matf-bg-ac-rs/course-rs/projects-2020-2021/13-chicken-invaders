#ifndef PAUSE_H
#define PAUSE_H

#include <QMediaPlayer>
#include <QPaintEvent>
#include <QPainter>
#include <QStyleOption>
#include <QWidget>

namespace Ui
{
class Pause;
}

class Pause : public QWidget
{
    Q_OBJECT

public:
    explicit Pause(QWidget* parent = nullptr);
    ~Pause();
    void paintEvent(QPaintEvent*);

private slots:
    void on_resumeButton_clicked();
    void on_goToMenuButton_clicked();
    void on_saveButton_clicked();
    void on_exitButton_clicked();

private:
    Ui::Pause* ui;
};

#endif // PAUSE_H
