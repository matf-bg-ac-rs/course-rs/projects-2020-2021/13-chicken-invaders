#ifndef CONTINUE_H
#define CONTINUE_H

#include <QMediaPlayer>
#include <QWidget>

namespace Ui
{
class Continue;
}

class Continue : public QWidget
{
    Q_OBJECT

public:
    explicit Continue(QWidget* parent = nullptr);
    ~Continue();
    void paintEvent(QPaintEvent*);

private slots:
    void on_openNewGameButton_clicked();
    void on_cancelButton_clicked();

private:
    Ui::Continue* ui;
};

#endif // CONTINUE_H
