#ifndef MENU_H
#define MENU_H

#include <QDir>
#include <QMainWindow>
#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <iostream>

namespace Ui
{
class Menu;
}

class Menu : public QMainWindow
{
    Q_OBJECT

public:
    int volume = 50;
    bool muted = false;
    int menu_width, menu_height;

    explicit Menu(QWidget* parent = nullptr);
    ~Menu();
    void show_score(int winOrLose);
    void disable_continueButton();
    void enable_continueButton();

private slots:
    void on_NewGameButton_clicked();
    void on_ResumeButton_clicked();
    void on_howToPlayButton_clicked();
    void on_optionButton_clicked();
    void on_exitButton_clicked();
    void on_goToHallOfFameButton_clicked();

    void on_credits_clicked();

private:
    Ui::Menu* ui;
};

#endif // MENU_H
