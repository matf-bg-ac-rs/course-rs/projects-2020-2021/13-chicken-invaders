#ifndef HALL_OF_FAME_H
#define HALL_OF_FAME_H

#include <QMediaPlayer>
#include <QPaintEvent>
#include <QPainter>
#include <QStyleOption>
#include <QWidget>

namespace Ui
{
class HallOfFame;
}

class HallOfFame : public QWidget
{
    Q_OBJECT

public:
    explicit HallOfFame(QWidget* parent = nullptr);
    ~HallOfFame();
    void paintEvent(QPaintEvent*);

public:
    void addPlayer(QString& name, int score, int level);
    void readFromFileAndInsertIntoList();
    void sortPlayersByScore();
    void insertPlayerIntoList(QString name, int score, int level);
    void printPlayersIntoFile();
    void printListToHallOfFame();

private slots:
    void on_goToMenuButton_clicked();

private:
    Ui::HallOfFame* ui;
    QList<std::tuple<QString, int, int>> playerList;
};

#endif // HALL_OF_FAME_H
