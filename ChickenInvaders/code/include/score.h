#ifndef SCORE_H
#define SCORE_H

#include <QMainWindow>
#include <QMediaPlayer>
#include <QPaintEvent>
#include <QPainter>
#include <QStyleOption>

namespace Ui
{
class score;
}

class score : public QMainWindow
{
    Q_OBJECT

public:
    explicit score(QWidget* parent = nullptr);
    ~score();

    void setParametarForGameWinOrLose(int param);
    void paintEvent(QPaintEvent*);

private slots:
    void on_saveResultButton_clicked();
    void on_goToMenuButton_clicked();
    void on_playAgainButton_clicked();
    void on_exitButton_clicked();

private:
    Ui::score* ui;
};

#endif // SCORE_H
