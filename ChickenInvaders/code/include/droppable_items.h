#ifndef EGG_H
#define EGG_H

#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include <QMediaPlayer>
#include <QObject>
#include <QTimer>

#define EGG_WIDTH (12)
#define EGG_HEIGHT (18)

class Egg : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    Egg(int x, int y);
    void destroy();

protected:
    const int egg_speed = 3;
    void advance(int a) override;
};

class Gift : public Egg
{
    Q_OBJECT
public:
    Gift(int x, int y);

protected:
    void advance(int a) override;
};

class Food : public Egg
{
    Q_OBJECT
public:
    Food(int x, int y);
    bool roast = false;

protected:
    const int food_speed = 2;
    void advance(int a) override;
};

class BossEgg : public Egg
{
    Q_OBJECT
public:
    BossEgg(int x, int y, int v_x, int v_y);
    void destroy();

protected:
    int vec_x, vec_y;
    void advance(int a) override;
};

#endif // EGG_H
