#ifndef OPTIONS_H
#define OPTIONS_H

#include <QMediaPlayer>
#include <QPaintEvent>
#include <QPainter>
#include <QStyleOption>
#include <QWidget>

namespace Ui
{
class options;
}

class options : public QWidget
{
    Q_OBJECT

public:
    explicit options(QWidget* parent = nullptr);
    ~options();
    void paintEvent(QPaintEvent*);

private slots:
    void on_save_clicked();
    void on_back_clicked();

private:
    Ui::options* ui;
};

#endif // OPTIONS_H
