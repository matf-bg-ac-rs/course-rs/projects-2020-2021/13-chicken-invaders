#ifndef ROCKET_H
#define ROCKET_H

#include <QFont>
#include <QGraphicsItem>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include <QList>
#include <QObject>
#include <QTimer>

#define RC_INDICATOR_WIDTH (60)
#define RC_INDICATOR_HEIGHT (66)

class Rocket : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    double currentStep = 0;
    double stepNumber = 45;
    double stepX;
    double stepY;

    Rocket();
    void destroy();
    void sound();

protected:
    void advance(int a) override;
};

#endif // ROCKET_H
