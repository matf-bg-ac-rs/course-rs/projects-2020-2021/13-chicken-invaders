#ifndef INSTRUCTIONS_H
#define INSTRUCTIONS_H

#include <QMediaPlayer>
#include <QPaintEvent>
#include <QPainter>
#include <QStyleOption>
#include <QWidget>

namespace Ui
{
class instructions;
}

class instructions : public QWidget
{
    Q_OBJECT

public:
    explicit instructions(QWidget* parent = nullptr);
    ~instructions();
    void paintEvent(QPaintEvent*);

private slots:
    void on_goBackPushButton_clicked();

private:
    Ui::instructions* ui;
};

#endif // INSTRUCTIONS_H
