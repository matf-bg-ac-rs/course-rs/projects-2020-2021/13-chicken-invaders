#ifndef GAME_H
#define GAME_H

#include <QApplication>
#include <QDesktopWidget>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QImage>
#include <QMediaPlayer>
#include <QScreen>
#include <QTimer>

#include "cleaner.h"
#include "level_manager.h"
#include "menu.h"
#include "player.h"
#include "player_hud.h"

#define PLAYER_WIDTH (60)
#define PLAYER_HEIGHT (66)
#define HP_SIZE (50)
#define FPS (1000 / 30)
#define TOPSPACEING (67)

class Game : public QGraphicsView
{
public:
    int game_width = 1200;
    int game_height = 800;

    // main timer
    QTimer timer;
    // clean up timer
    QTimer clean_up_time;
    // new parent for objects that are removed from scene
    Cleaner* cleaner = new Cleaner();

    // media players
    QMediaPlayer* laserSound;
    QMediaPlayer* chickenSound;
    QMediaPlayer* meteorSound;
    QMediaPlayer* rocketSound;
    QMediaPlayer* bossSound;

    LevelManager* lm;
    QGraphicsScene* game;
    Player* player;
    PlayerScore* player_score;
    QVector<PlayerHealth*> health_bar;
    QVector<RocketCountIndikator*> rocket_bar;

    Game();
    ~Game();
    void resetGame();
    void setGameVolume();

private:
    void cleanUpRestart();
};

#endif // GAME_H
