#ifndef LEVER_MANAGER_H
#define LEVER_MANAGER_H

#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include <QMediaPlayer>
#include <QObject>
#include <random>

#include "meteor.h"

class LevelManager : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    int level_counter;
    bool move_direction;

    LevelManager();
    ~LevelManager();
    void waveSub();
    void resetLM();
    void ContinueFromPause();
    void nextLevel();
    void setScreenDiagonal(int x, int y);

private:
    int wave_counter;
    int enemy_number;
    int counter;

    int block_spawn_speed = 100;

    int h_screen_diagonal = 1000; // half the diagonal of the screen
    const int meteor_buffer = 80; // buffer the size of the largest meteor

    QTimer* timer_block;
    QTimer* timer_fall;
    QTimer* timer_meteors;
    QTimer* timer_left_right;

    // Block, Fall,Meteor,Block, L2R, Block, Meteor, R2L, Fall, Boss
    const std::vector<int> block_levels = {1, 4, 6};
    const std::vector<int> fall_levels = {2, 9};
    const std::vector<int> left_right_levels = {5, 8};
    const std::vector<int> meteors_levels = {3, 7};

    // flags for spawn left_right chicken; 0-free;1-not
    int lanes[6] = {0, 0, 0, 0, 0, 0};

    void spawnChicken(int x, int y);
    void spawnBoss();
    void spawnMeteor(int x, int y);

    void setScaledBackground(const QString& path);

public slots:
    void manager();
    void spawn_block();
    void spawn_fall();
    void spawnFallMeteors();
    void spawn_left_right();
};

#endif // LEVER_MANAGER_H
