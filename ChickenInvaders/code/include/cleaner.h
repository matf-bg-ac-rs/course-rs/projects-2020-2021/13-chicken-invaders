#ifndef CLEANER_H
#define CLEANER_H

#include <QGraphicsItem>
#include <QObject>

class Cleaner : public QObject
{
    Q_OBJECT
public:
    Cleaner();
    QVector<QGraphicsItem*>* cleaner;
public slots:
    void cleanUp();
};

#endif // CLEANER_H
