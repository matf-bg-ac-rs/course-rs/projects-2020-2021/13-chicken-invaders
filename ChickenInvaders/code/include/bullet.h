#ifndef BULLET_H
#define BULLET_H

#include <QGraphicsItem>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include <QList>
#include <QObject>
#include <QTimer>

#define BULLET_HEIGHT (28)
#define BULLET_WIDTH (10)

class Bullet : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
private:
    const int speed = 6;
    int angle = 0; // in degress
    int strength = 1;

public:
    Bullet(int alfa, float scale);
    void destroy();

protected:
    void advance(int a) override;
};

#endif // BULLET_H
