#ifndef PLAYER_SCORE_H
#define PLAYER_SCORE_H

#include <QFont>
#include <QGraphicsPixmapItem>
#include <QGraphicsTextItem>

// HUD - Heads-up display

class PlayerScore : public QGraphicsTextItem
{
private:
    int score;
    void drawText();
    int new_life_score = 10000;

public:
    int food_eaten = 0;
    int chicken_points = 10;
    int chicken_points_death = 50;
    int meteor_points = 100;
    int boss_points = 50;
    int boss_points_death = 1000;
    const int gift_points = 10;
    const int food_points = 25;
    int level = 1;

    PlayerScore(QGraphicsItem* parent = 0);
    void increseScore(int increse);
    int getScore();
    void resetScore();
    void setPointsOnLevelUp();
    void addFoodEaten(int n);
};

class PlayerHealth : public QGraphicsPixmapItem
{
public:
    PlayerHealth();
};

class RocketCountIndikator : public QGraphicsPixmapItem
{
public:
    RocketCountIndikator();
};

#endif // PLAYER_SCORE_H
