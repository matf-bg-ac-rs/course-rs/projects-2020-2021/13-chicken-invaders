#ifndef BOSS_H
#define BOSS_H

#include <QGraphicsPixmapItem>
#include <QGraphicsRectItem>
#include <QGraphicsScene>
#include <QLabel>
#include <QList>
#include <QMovie>
#include <QObject>
#include <QTimer>
#include <QtMath>

#include <algorithm>
#include <stdlib.h>

#define BOSS_WIDTH (300)
#define BOSS_HEIGHT (259)

class Boss : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    int special_hp = 50;
    int hp = 50;
    double speed_movement = 0.5;
    double direction_x = 1, direction_y = 1;
    int wing_speed = 25;
    int flap_wing = 0;
    Boss(int x, int y);
    void destroy();
    void haveToDie();
    void dieSound();
    int getHp();
    void decreaseHp(int x);
    void rocketHit();

private:
    int shootParametar = 0;
    bool shootInCircles = false;
    bool entrance_complete = false;
    int radius = 50;
    QPixmap boss_1 = QPixmap(":/images/boss1");
    QPixmap boss_2 = QPixmap(":/images/boss2");
    void movement();
    void shoot();
    void drop_food();
    void drop_gift();
    void flapWings();
    void createGift(int x, int y);
    void drawCirclesOfEggs(int x, int y);
    void entrance_movement();

protected:
    int shot_counter = 0;
    void advance(int a) override;
    void spawnBossEgg(int x, int y, int v_x, int v_y);
    void shootEggsInCircles();
    void shootEggsSpecial();
};

#endif // BOSS_H
