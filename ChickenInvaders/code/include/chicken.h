#ifndef CHICKEN_H
#define CHICKEN_H

#include <QGraphicsPixmapItem>
#include <QGraphicsRectItem>
#include <QGraphicsScene>
#include <QLabel>
#include <QList>
#include <QMovie>
#include <QObject>
#include <QTimer>
#include <algorithm>
#include <stdlib.h>

const int movement_speed_init = 1;

class Chicken : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    Chicken(int x, int y);
    void destroy();
    void haveToDie();
    void dieSound();

    int egg_level;
    int movement_type;
    int hp = 2;
    int speed_movement;

private:
    QPixmap chicken_1 = QPixmap(":/images/ch1");
    QPixmap chicken_2 = QPixmap(":/images/ch2");

    int flap_wing = 0;
    int wing_speed = 30;
    int shoot_egg_probability = 6;

    void move_block();
    void move_fall();
    void move_left_right();

protected:
    void FlapWings();
    void drop_gifts();
    void drop_food();
    void advance(int a) override;

public slots:
    void movement();
    void decreaseHp(int x);
    void shoot();
};

#endif // CHICKEN_H
