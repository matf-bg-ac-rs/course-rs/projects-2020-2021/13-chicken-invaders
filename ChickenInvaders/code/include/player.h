#ifndef PLAYER_H
#define PLAYER_H
#include <vector>

#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include <QKeyEvent>
#include <QLabel>
#include <QMediaPlayer>
#include <QObject>
#include <QTimer>

#include "code/include/rocket.h"

#define BIG_BULLET (1.3)

class Player : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
private:
    const int speed = 4;
    int health = 3;
    const int cooldown_time = 250; // 1/4 sec
    const int rocket_cooldown_time = 1500;
    const int num_food_new_rocket = 150;
    int weapon_lvl = 0;
    int rocket_count = 1;
    bool fire_cooldown = false;
    bool rocket_cooldown = false;
    bool shooting = false;
    bool alive = true;
    bool haveShield = false;
    bool move_left = false;
    bool move_right = false;
    bool move_up = false;
    bool move_down = false;

    QTimer* cooldown_timer = new QTimer();
    QTimer* rocket_cooldown_timer = new QTimer();
    QTimer* shield_timer = new QTimer();

    QPixmap player_image = QPixmap(":/images/player");
    QPixmap player_shield_image = QPixmap(":/images/p_shield");

    void reducePlayerHealth();
    void fireWeapon();
    void spawnBullet(int x, int y, int alpha, float scale);
    void movement();

public:
    Player();
    ~Player();
    void keyPressEvent(QKeyEvent* event) override;
    void keyReleaseEvent(QKeyEvent* event) override;
    int getPlayerHealth();
    void addPlayerHealth();
    int getPlayerRockets();
    void addPlayerRockets();
    void reducePlayerRockets();

    void FireRocket();
    void RocketShot();
    void goToNextLevel();
    void EatFood(int n);
    void resetPlayer();
    void playerDeath();
    int getPlayerWeaponLevel();

protected:
    void advance(int a) override;

public slots:
    void resetCooldownFire();
    void removeShield();
    void resetRocketCooldownFire();
};

#endif // PLAYER_H
