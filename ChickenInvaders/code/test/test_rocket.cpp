#include "../include/catch.hpp"
#include "../include/rocket.h"
#include "../include/game.h"

Game* game = nullptr;

TEST_CASE("Rocket class"){
     game = new Game();

    SECTION("Create rocket at default position"){
        Rocket* rocket = new Rocket();

        REQUIRE(rocket != nullptr);
        delete rocket;
    }

    SECTION("Change rocket position"){
        Rocket* rocket = new Rocket();
        int x=100, y=150;
        rocket->setPos(x, y);

        REQUIRE(rocket->x() == x && rocket->y() == 7);
        delete rocket;
    }

    SECTION("Change rocket step"){
        Rocket* rocket = new Rocket();
        int x=6, y=1;
        rocket->stepY = x;
        rocket->stepX = y;

        REQUIRE(rocket->stepX == x && rocket->stepY == y);
        delete rocket;
    }

    SECTION("Rocket sound check"){
        new Rocket();
        REQUIRE(game->rocketSound->state() == QMediaPlayer::PlayingState);
    }

    delete game;
}
