#include "code/include/credits.h"
#include "code/include/menu.h"
#include "ui_credits.h"

extern Menu* menu;

Credits::Credits(QWidget* parent)
    : QWidget(parent)
    , ui(new Ui::Credits)
{
    ui->setupUi(this);

    // set a background for larger resolution screens then the video resolution
    QPixmap bkgnd(":/images/menu_background");
    QPalette palette;
    palette.setBrush(QPalette::Background, bkgnd);
    this->setPalette(palette);

    // create and play Credits giff
    QMovie* movie = new QMovie(":/images/credits_gif");
    QLabel* processLabel = new QLabel(this);
    processLabel->setMovie(movie);
    const int rez_width = 720; // half the width of the gif
    const int rez_height = 400; // half the height of the gif
    // center gif on screen
    processLabel->move(menu->menu_width / 2 - rez_width,
        menu->menu_height / 2 - rez_height);
    movie->start();
}

Credits::~Credits() { delete ui; }
