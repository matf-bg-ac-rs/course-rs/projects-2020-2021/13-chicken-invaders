#include "code/include/rocket.h"
#include "code/include/boss.h"
#include "code/include/bullet.h"
#include "code/include/chicken.h"
#include "code/include/game.h"

#define SCALE_ROCKET (0.75)

extern Game* game;

Rocket::Rocket()
{
    setPixmap(QPixmap(":/images/rainder_rocket"));
    setScale(SCALE_ROCKET);
    sound();
    currentStep = 0;
}

void Rocket::destroy()
{
    scene()->removeItem(this);
    game->cleaner->cleaner->append(this);
}

// function connect to timer
void Rocket::advance(int a)
{
    (void)a;

    if (currentStep++ <= stepNumber) {
        setPos(x() + stepX, y() + stepY);
        setScale(SCALE_ROCKET + currentStep * 0.01);
    } else if (currentStep >= stepNumber) {
        game->player->RocketShot();
        destroy();
    }
}

// sound effect for fired rocket
void Rocket::sound()
{
    if (game->rocketSound->state() == QMediaPlayer::PlayingState) {
        game->rocketSound->setPosition(0);
    }
    game->rocketSound->play();
}
