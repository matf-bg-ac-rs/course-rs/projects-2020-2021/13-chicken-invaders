#include "code/include/credits.h"
#include "code/include/game.h"
#include "code/include/menu.h"
#include "code/include/score.h"
#include <QMediaPlayer>

#include <iostream>

// unique instance of menu, heart of whole app
// parent to the all other elements of app
Menu* menu;

int main(int argc, char* argv[])
{
    QApplication app(argc, argv);

    // make new menu and show
    menu = new Menu();
    menu->showMaximized();

    return app.exec();
}
