﻿#include "code/include/level_manager.h"
#include "code/include/boss.h"
#include "code/include/chicken.h"
#include "code/include/game.h"
#include "code/include/meteor.h"

#define BLOCK_SPAWN_SPEED (100)
#define FALL_SPAWN_SPEED (2000)
#define FALL_METEOR_SPEED (4000)
#define LEFT_RIGHT_SPAWN_SPEED (2000)

extern Game* game;
extern bool pause;

struct compare {
    int key;
    compare(int const& i)
        : key(i)
    {
    }

    bool operator()(int const& i) { return (key == i); }
};

LevelManager::LevelManager()
{
    // init values
    level_counter = 1; // start:1
    wave_counter = 0;
    enemy_number = 40;
    counter = 0;
    move_direction = false;

    // init timers
    timer_block = new QTimer();
    timer_fall = new QTimer();
    timer_meteors = new QTimer();
    timer_left_right = new QTimer();

    // connect timers
    connect(timer_block, SIGNAL(timeout()), this, SLOT(spawn_block()));
    connect(timer_fall, SIGNAL(timeout()), this, SLOT(spawn_fall()));
    connect(timer_meteors, SIGNAL(timeout()), this, SLOT(spawnFallMeteors()));
    connect(timer_left_right, SIGNAL(timeout()), this,
        SLOT(spawn_left_right()));
}

LevelManager::~LevelManager()
{
    delete timer_block;
    delete timer_fall;
    delete timer_meteors;
    delete timer_left_right;
}

// main func. for selecting level and spawn correct enemy quantity,size and
// types
void LevelManager::manager()
{
    if (level_counter == 101) {
        // call END GAME WIN script
        game->player->playerDeath();
    } else if (std::any_of(block_levels.begin(), block_levels.end(),
                   compare(level_counter % 10))) {
        enemy_number = 40;
        wave_counter = enemy_number;
        timer_block->start(block_spawn_speed - level_counter / 2);
    } else if (std::any_of(fall_levels.begin(), fall_levels.end(),
                   compare(level_counter % 10))) {
        enemy_number = 10 * (level_counter / 5 + 6);
        wave_counter = enemy_number;
        timer_fall->start(FALL_SPAWN_SPEED);
        setScaledBackground(":/images/block_background");
    } else if (std::any_of(meteors_levels.begin(), meteors_levels.end(),
                   compare(level_counter % 10))) {
        enemy_number = 500;
        wave_counter = enemy_number;
        timer_meteors->start(FALL_METEOR_SPEED);
        setScaledBackground(":/images/meteor_background");
    } else if (std::any_of(left_right_levels.begin(), left_right_levels.end(),
                   compare(level_counter % 10))) {
        enemy_number = 60;
        wave_counter = enemy_number;
        for (int i = 0; i < 6; i++) {
            lanes[i] = 0;
        }
        timer_left_right->start(LEFT_RIGHT_SPAWN_SPEED);

        setScaledBackground(":/images/bg1");
    } else if (level_counter % 10 == 0) {
        enemy_number = 1;
        wave_counter = enemy_number;
        spawnBoss();
        setScaledBackground(":/images/boss_background");
    }
}

// looking after counter of alive enemy
// when is zero go to next level
void LevelManager::waveSub()
{
    wave_counter--;
    if (wave_counter == 0) {
        level_counter++;
        game->player_score->setPointsOnLevelUp();
        counter = 0;
        manager();
    }
}

// reseting for new game
void LevelManager::resetLM()
{
    level_counter = 1;
    wave_counter = 0;
    enemy_number = 40;

    move_direction = false;
    counter = 0;
}

// when pause is lifted do this
void LevelManager::ContinueFromPause()
{
    pause = false;
    if (std::any_of(block_levels.begin(), block_levels.end(),
            compare(level_counter % 10))
        && counter != enemy_number) {
        timer_block->start(block_spawn_speed - level_counter / 2);
    } else if (std::any_of(fall_levels.begin(), fall_levels.end(),
                   compare(level_counter % 10))
        && counter != enemy_number) {
        timer_fall->start(FALL_SPAWN_SPEED);
    } else if (std::any_of(meteors_levels.begin(), meteors_levels.end(),
                   compare(level_counter % 10))
        && counter != enemy_number) {
        timer_meteors->start(FALL_METEOR_SPEED);
    } else if (std::any_of(left_right_levels.begin(), left_right_levels.end(),
                   compare(level_counter % 10))
        && counter != enemy_number) {
        timer_left_right->start(LEFT_RIGHT_SPAWN_SPEED);
    }
}

// part of cheats: next level, just for dev and testing
void LevelManager::nextLevel()
{
    wave_counter = 1;
    if (timer_block->isActive()) {
        timer_block->stop();
    }
    if (timer_fall->isActive()) {
        timer_fall->stop();
    }
    if (timer_meteors->isActive()) {
        timer_meteors->stop();
    }
    if (timer_left_right->isActive()) {
        timer_left_right->stop();
    }

    waveSub();
}

void LevelManager::setScreenDiagonal(int x, int y)
{
    h_screen_diagonal = sqrt(pow(x, 2) + pow(y, 2)) / 2;
}

// make 8x5 block of chicken
void LevelManager::spawn_block()
{
    if (pause) {
        timer_block->stop();
    } else if (!timer_block->isActive()) {
        timer_block->start(BLOCK_SPAWN_SPEED);
    }

    if (counter < enemy_number && !pause) {
        // maximum distance a chicken in block formation can travel
        // Nikola-Bojan constant
        int nb_cons = game->game_width - 110;
        int speed_const = BLOCK_SPAWN_SPEED / (FPS / 2);

        int x = (counter * speed_const) % (nb_cons);
        spawnChicken(10 + x + (counter % 8) * 110, 80 + (counter / 8) * 80);

        counter++;
    }
    if (counter >= enemy_number) {
        timer_block->stop();
    }

    game->setBackgroundBrush(QBrush(QImage(":/images/bg10")));
}

// make row of falling chickens at random width
void LevelManager::spawn_fall()
{
    if (pause) {
        timer_fall->stop();
    } else if (!timer_fall->isActive()) {
        timer_fall->start(FALL_SPAWN_SPEED);
    }

    int x = (rand() % (game->game_width / 2));

    if (!pause) {
        for (int i = 0; i < 10; i++) {
            spawnChicken(10 + x + i * 100, -100);
            counter++;
            if (counter % 10 == 0) {
                break;
            }
        }
    }
    if (counter >= enemy_number) {
        timer_fall->stop();
    }
}

// make row of sliding left_right chickens at random height
// lvl=5 => from left to right; lvl=8 => from right to left
void LevelManager::spawn_left_right()
{
    if (pause) {
        timer_left_right->stop();
    } else if (!timer_left_right->isActive()) {
        timer_left_right->start(LEFT_RIGHT_SPAWN_SPEED);
    }

    int ranPoz = rand() % 6;

    bool has_free_lane = !std::accumulate(std::begin(lanes), std::end(lanes), 1,
        std::logical_and<int> {});
    if (counter >= enemy_number)
        return;

    while (lanes[ranPoz] == 1 && has_free_lane) {
        // 0-free;1-not
        ranPoz = (ranPoz + 1) % 6;
    }

    int y = 65 + 95 * ranPoz;

    if (!pause) {
        for (int i = 0; i < 10; i++) {
            if (level_counter % 10 == 5) {
                // lvl=5 => from left to right
                spawnChicken(-10 - i * 135 - 10, y);
            } else if (level_counter % 10 == 8) {
                // lvl=8 => from right to left
                spawnChicken(game->game_width + i * 135 + 10, y);
            }
        }
        counter += 10;
        lanes[ranPoz] = 1;
    }
    if (counter >= enemy_number) {
        timer_left_right->stop();
    }
}

// spawn one chicken
void LevelManager::spawnChicken(int x, int y)
{
    Chicken* chicken = new Chicken(x, y);
    scene()->addItem(chicken);
}

// spawn one boss
void LevelManager::spawnBoss()
{
    Boss* boss = new Boss(game->game_width / 2 - BOSS_WIDTH / 2, -BOSS_HEIGHT);
    scene()->addItem(boss);
}

// spawn one meteor
void LevelManager::spawnMeteor(int x, int y)
{
    Meteor* meteor = new Meteor(x, y);
    scene()->addItem(meteor);
}

// scale background image
void LevelManager::setScaledBackground(const QString& path)
{

    QSize widgetSize = game->size();
    QPixmap pixmap;
    pixmap.load(path);
    QPixmap scaledPixmap = pixmap.scaled(widgetSize, Qt::IgnoreAspectRatio);
    game->setBackgroundBrush(scaledPixmap);
}

// spawn of meteors for meteors lvl
void LevelManager::spawnFallMeteors()
{
    if (pause) {
        timer_meteors->stop();
    } else if (!timer_meteors->isActive()) {
        timer_meteors->start(FALL_METEOR_SPEED);
    }

    const int number_in_wave = 15 + rand() % 15;

    if (!pause) {
        for (int i = 0; i <= number_in_wave; i++) {
            const int buffered_hsd = h_screen_diagonal + meteor_buffer + (rand() % meteor_buffer);
            const int spMeteorX = (rand() % (buffered_hsd));
            const int spMeteorY = game->game_height / 2 - sqrt(pow(buffered_hsd, 2) - pow(spMeteorX, 2));
            spawnMeteor(game->game_width / 2 - spMeteorX, spMeteorY);
            counter++;
            if (counter % number_in_wave == 0) {
                break;
            }
        }
    }

    if (counter >= enemy_number) {
        timer_meteors->stop();
    }
}
