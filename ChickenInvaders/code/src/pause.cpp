#include "code/include/pause.h"
#include "code/include/game.h"
#include "code/include/menu.h"
#include "ui_pause.h"

extern Game* game;
extern Menu* menu;
extern QMediaPlayer* music_player;
extern bool pause;

Pause::Pause(QWidget* parent)
    : QWidget(parent)
    , ui(new Ui::Pause)
{
    ui->setupUi(this);

    ui->resumeButton->move(
        menu->menu_width / 2 - ui->resumeButton->width() / 2 - ui->resumeButton->width() + ui->resumeButton->width() / 3 + 40,
        menu->menu_height / 2 - ui->resumeButton->height() / 2);
    ui->saveButton->move(menu->menu_width / 2 - ui->saveButton->width() / 2 + ui->saveButton->width() / 2 + 40,
        menu->menu_height / 2 - ui->saveButton->height() / 2);

    ui->goToMenuButton->move(
        menu->menu_width / 2 - ui->resumeButton->width() / 2 - ui->resumeButton->width() + ui->resumeButton->width() / 3 + 40,
        menu->menu_height / 2 + 3 * ui->exitButton->height() / 4);
    ui->exitButton->move(menu->menu_width / 2 - ui->saveButton->width() / 2 + ui->saveButton->width() / 2 + 40,
        menu->menu_height / 2 + 3 * ui->exitButton->height() / 4);

    ui->volume->move(menu->menu_width / 2 - ui->volume->width(),
        menu->menu_height / 2 - ui->soundSlider->height() / 2 - 5 * ui->resumeButton->height() / 4);
    ui->mute->move(menu->menu_width / 2 - ui->volume->width(),
        menu->menu_height / 2 - ui->soundSlider->height() / 2 - 7 * ui->resumeButton->height() / 4);

    ui->soundSlider->move(menu->menu_width / 2 + ui->soundSlider->width() / 5,
        menu->menu_height / 2 - ui->soundSlider->height() / 2 - 13 * ui->resumeButton->height() / 12);
    ui->muteCheckBox->move(menu->menu_width / 2 + ui->soundSlider->width() / 5,
        menu->menu_height / 2 - ui->soundSlider->height() / 2 - 19 * ui->resumeButton->height() / 12 + 7);

    if (menu->muted) {
        ui->muteCheckBox->setChecked(true);
        ui->soundSlider->setValue(0);
    } else {
        ui->soundSlider->setValue(menu->volume);
    }
}

Pause::~Pause() { delete ui; }

// resume with the game
void Pause::on_resumeButton_clicked()
{
    if (ui->muteCheckBox->isChecked()) {
        menu->volume = 0;
        menu->muted = true;
        music_player->setVolume(0);
        game->setGameVolume();
    } else {
        menu->volume = ui->soundSlider->value();
        music_player->setVolume(menu->volume);
        game->setGameVolume();
        menu->muted = false;
    }

    game->showMaximized();
    game->lm->ContinueFromPause();

    this->close();
}

// go to menu
void Pause::on_goToMenuButton_clicked()
{
    menu->showMaximized();
    game->hide();
    this->close();
}

// save changes
void Pause::on_saveButton_clicked()
{
    menu->muted = ui->muteCheckBox->isChecked();
    if (menu->muted) {
        ui->soundSlider->setValue(0);
    }
    menu->volume = ui->soundSlider->value();
    game->setGameVolume();
    music_player->setVolume(menu->volume);
}

// exit from app
void Pause::on_exitButton_clicked() { exit(0); }

void Pause::paintEvent(QPaintEvent*)
{
    QStyleOption opt;
    opt.init(this);
    QPainter p(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
}
