#include "code/include/droppable_items.h"
#include "code/include/game.h"

extern Game* game;
extern bool pause;

Egg::Egg(int x, int y)
    : QObject()
{
    setPixmap(QPixmap(":/images/egg"));
    setPos(x, y);
}

// function connect to timer, controls movement
void Egg::advance(int a)
{
    (void)a;
    if (!pause) {
        if (y() < game->game_height)
            setPos(x(), y() + egg_speed);
        else {
            destroy();
        }
    }
}

void Egg::destroy()
{
    scene()->removeItem(this);
    game->cleaner->cleaner->append(this);
}

Gift::Gift(int x, int y)
    : Egg(x, y)
{
    setPixmap(QPixmap(":/images/gift2"));
    setScale(0.7);
}

// function connected to timer, controls movement
void Gift::advance(int a)
{
    (void)a;
    if (!pause) {
        if (y() < game->game_height)
            setPos(x(), y() + egg_speed);
        else {
            destroy();
        }
    }
}

Food::Food(int x, int y)
    : Egg(x, y)
{
    int randRoast = (std::rand() % 100);
    if (randRoast <= 5) {
        setPixmap(QPixmap(":/images/roast"));
        const int roast_width = 40, roast_height = 30;
        setTransformOriginPoint(roast_width / 2, roast_height / 2);
        roast = true;
    } else {
        setPixmap(QPixmap(":/images/food"));
        setScale(0.5);
        const int food_width = 38, food_height = 66;
        setTransformOriginPoint(food_width / 2, food_height / 2);
        int food_rotation = rand() % 360;
        setRotation(food_rotation);
        roast = false;
    }
    setPos(x, y);
}

// function connect to timer, controls movement
void Food::advance(int a)
{
    (void)a;
    if (!pause) {
        if (y() < game->game_height) {
            setPos(x(), y() + food_speed);
            setRotation(rotation() + 2);
        } else {
            destroy();
        }
    }
}

BossEgg::BossEgg(int x, int y, int v_x, int v_y)
    : Egg(x, y)
{
    setScale(1.2);
    vec_x = v_x;
    vec_y = v_y;
}

void BossEgg::destroy()
{
    scene()->removeItem(this);
    game->cleaner->cleaner->append(this);
}

// function connect to timer, controls movement
void BossEgg::advance(int a)
{
    (void)a;
    if (!pause) {
        if (y() < game->game_height && y() > -EGG_HEIGHT && x() > -EGG_WIDTH && x() < game->game_width) {
            setPos(x() + vec_x, y() + vec_y);
        } else {
            destroy();
        }
    }
}
