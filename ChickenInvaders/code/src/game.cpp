﻿#include "code/include/game.h"
#include "code/include/boss.h"
#include "code/include/bullet.h"
#include "code/include/chicken.h"
#include "code/include/droppable_items.h"
#include "code/include/level_manager.h"

// val for pausing every movement and opening pause menu when user clik Esc
bool pause = false;
extern Menu* menu;

Game::Game()
{
    // Init graphic scene
    game = new QGraphicsScene();
    // We take the size of the main screen
    QScreen* screen = QGuiApplication::primaryScreen();
    QRect rec = screen->geometry();
    game_height = rec.height();
    game_width = rec.width();

    this->resize(game_width, game_height);
    // we are adding a small buffer to the edges of the screen for the scene
    game_height *= 0.96;
    game_width *= 0.97;

    game->setSceneRect(0, 0, game_width, game_height);
    setBackgroundBrush(QBrush(QImage(":/images/bg9")));

    // Set current scene to main scene
    setScene(game);

    game->setItemIndexMethod(QGraphicsScene::NoIndex);

    // Init player
    player = new Player();

    // Focusing on the player item
    player->setFlag(QGraphicsItem::ItemIsFocusable);
    player->setFocus();

    // init hp
    player_score = new PlayerScore();
    for (int i = 0; i < player->getPlayerHealth(); i++) {
        // Show HP
        health_bar.append(new PlayerHealth());
        health_bar[i]->setPos(game_width - (i + 1) * (HP_SIZE + 5), 5);
        game->addItem(health_bar[i]);
    }

    // init rocket
    rocket_bar.append(new RocketCountIndikator());
    rocket_bar[0]->setPos(game_width - (RC_INDICATOR_WIDTH + 10),
        TOPSPACEING - 5);
    game->addItem(rocket_bar[0]);

    // Add player and score
    game->addItem(player);
    game->addItem(player_score);

    // Disabling scroll bars
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    // Drawing Player
    setWindowTitle("Chicken Invaders");
    player->setPos(game_width / 2 - PLAYER_WIDTH / 2,
        game_height - PLAYER_HEIGHT * 1.5);

    // Main game timer
    QObject::connect(&timer, SIGNAL(timeout()), game, SLOT(advance()));
    timer.start(FPS);

    // Setting media for the game
    laserSound = new QMediaPlayer();
    laserSound->setMedia(QUrl("qrc:/sounds/laser_sound"));

    chickenSound = new QMediaPlayer();
    chickenSound->setMedia(QUrl("qrc:/sounds/chicken"));

    meteorSound = new QMediaPlayer();
    meteorSound->setMedia(QUrl("qrc:/sounds/meteor"));

    rocketSound = new QMediaPlayer();
    rocketSound->setMedia(QUrl("qrc:/sounds/rocket"));

    bossSound = new QMediaPlayer();
    bossSound->setMedia(QUrl("qrc:/sounds/boss"));

    setGameVolume();

    // init main level manager (control level and waves)
    lm = new LevelManager();
    lm->manager();
    game->addItem(lm);
    lm->setScreenDiagonal(game_width, game_height);

    QObject::connect(&clean_up_time, SIGNAL(timeout()), cleaner,
        SLOT(cleanUp()));
    clean_up_time.start(FPS);
}

Game::~Game()
{
    delete laserSound;
    delete chickenSound;
    delete meteorSound;
    delete rocketSound;
    delete bossSound;

    delete cleaner;
    delete lm;
    delete game;
    delete player;
    delete player_score;
    health_bar.clear();
    rocket_bar.clear();
}

// reset values and states needed for new game to start
void Game::resetGame()
{
    cleanUpRestart();
    player_score->resetScore();
    player->resetPlayer();
    lm->resetLM();
    lm->manager();
    setGameVolume();
}

// setting game volume
void Game::setGameVolume()
{
    int gm_volume = menu->volume;
    chickenSound->setVolume(gm_volume / 6);
    laserSound->setVolume(gm_volume / 6);
    meteorSound->setVolume(gm_volume / 2);
    rocketSound->setVolume(gm_volume);
    bossSound->setVolume(gm_volume);
}

// deleting all object that are on screen
void Game::cleanUpRestart()
{
    foreach (QGraphicsItem* item, items()) {
        if (typeid(*item) == typeid(Chicken)) {
            Chicken* chicken = qgraphicsitem_cast<Chicken*>(item);
            chicken->destroy();
        } else if (typeid(*item) == typeid(Egg)) {
            Egg* egg = qgraphicsitem_cast<Egg*>(item);
            egg->destroy();
        } else if (typeid(*item) == typeid(Bullet)) {
            Bullet* bullet = qgraphicsitem_cast<Bullet*>(item);
            bullet->destroy();
        } else if (typeid(*item) == typeid(Gift)) {
            Gift* gift = qgraphicsitem_cast<Gift*>(item);
            gift->destroy();
        } else if (typeid(*item) == typeid(Boss)) {
            Boss* boss = qgraphicsitem_cast<Boss*>(item);
            boss->destroy();
        } else if (typeid(*item) == typeid(Food)) {
            Food* food = qgraphicsitem_cast<Food*>(item);
            food->destroy();
        } else if (typeid(*item) == typeid(Meteor)) {
            Meteor* meteor = qgraphicsitem_cast<Meteor*>(item);
            meteor->destroy();
        } else if (typeid(*item) == typeid(BossEgg)) {
            BossEgg* begg = qgraphicsitem_cast<BossEgg*>(item);
            begg->destroy();
        }
    }
}
