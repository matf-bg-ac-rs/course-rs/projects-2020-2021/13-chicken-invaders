#include "code/include/cleaner.h"
#include "code/include/boss.h"
#include "code/include/bullet.h"
#include "code/include/chicken.h"
#include "code/include/droppable_items.h"
#include "code/include/meteor.h"

Cleaner::Cleaner() { cleaner = new QVector<QGraphicsItem*>(); }

// destroy all object that are not anymore in the scene
void Cleaner::cleanUp()
{
    while (!cleaner->isEmpty()) {
        QGraphicsItem* item = cleaner->last();
        cleaner->removeLast();
        if (typeid(*item) == typeid(Chicken)) {
            Chicken* chicken = qgraphicsitem_cast<Chicken*>(item);
            delete chicken;
        } else if (typeid(*item) == typeid(Egg)) {
            Egg* egg = qgraphicsitem_cast<Egg*>(item);
            delete egg;
        } else if (typeid(*item) == typeid(Bullet)) {
            Bullet* bullet = qgraphicsitem_cast<Bullet*>(item);
            delete bullet;
        } else if (typeid(*item) == typeid(Gift)) {
            Gift* gift = qgraphicsitem_cast<Gift*>(item);
            delete gift;
        } else if (typeid(*item) == typeid(Boss)) {
            Boss* boss = qgraphicsitem_cast<Boss*>(item);
            delete boss;
        } else if (typeid(*item) == typeid(Food)) {
            Food* food = qgraphicsitem_cast<Food*>(item);
            delete food;
        } else if (typeid(*item) == typeid(Meteor)) {
            Meteor* meteor = qgraphicsitem_cast<Meteor*>(item);
            delete meteor;
        } else if (typeid(*item) == typeid(BossEgg)) {
            BossEgg* begg = qgraphicsitem_cast<BossEgg*>(item);
            delete begg;
        }
    }
}
