#include "code/include/instructions.h"
#include "code/include/menu.h"
#include "ui_instructions.h"

extern Menu* menu;

instructions::instructions(QWidget* parent)
    : QWidget(parent)
    , ui(new Ui::instructions)
{
    ui->setupUi(this);
    ui->goBackPushButton->move(
        menu->menu_width / 2 - ui->goBackPushButton->width() / 2,
        menu->menu_height / 2 + 6 * ui->goBackPushButton->height() / 2);

    const int label_buffer = 600;
    ui->label->move(menu->menu_width / 2 - label_buffer,
        menu->menu_height / 2 - 3 * ui->fire->height() / 5);
    ui->fire->move(menu->menu_width / 2 + 20,
        menu->menu_height / 2 - 3 * ui->fire->height() / 5);
}

instructions::~instructions() { delete ui; }

// go back to menu
void instructions::on_goBackPushButton_clicked()
{
    menu->showMaximized();
    this->hide();
}

void instructions::paintEvent(QPaintEvent*)
{
    QStyleOption opt;
    opt.init(this);
    QPainter p(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
}
