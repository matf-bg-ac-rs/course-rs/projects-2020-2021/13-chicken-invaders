#include "code/include/continue.h"
#include "code/include/game.h"
#include "code/include/menu.h"
#include "ui_continue.h"

#include <QPaintEvent>
#include <QPainter>
#include <QStyleOption>

extern Menu* menu;
extern Game* game;
extern bool pause;

Continue::Continue(QWidget* parent)
    : QWidget(parent)
    , ui(new Ui::Continue)
{
    ui->setupUi(this);
    QPixmap bkgnd(":/images/re_play");
    QPalette palette;
    palette.setBrush(QPalette::Background, bkgnd);
    this->setPalette(palette);

    const int cont_height = 400;
    const int cont_width = 600;
    const int width = (QApplication::desktop()->width() - cont_width) / 2;
    const int height = (QApplication::desktop()->height() - cont_height) / 2;
    ;
    this->setGeometry(width, height, cont_width, cont_height);

    setStyleSheet("background-image: url(:images/continue_menu); "
                  "background-position: center;");
}

Continue::~Continue() { delete ui; }

// start a new game
void Continue::on_openNewGameButton_clicked()
{

    pause = false;
    game->resetGame();
    game->showMaximized();
    menu->enable_continueButton();
    menu->hide();
    this->close();
}

// go back to main, close this widget
void Continue::on_cancelButton_clicked() { this->close(); }

void Continue::paintEvent(QPaintEvent*)
{
    QStyleOption opt;
    opt.init(this);
    QPainter p(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
}
