#include "code/include/score.h"
#include "code/include/credits.h"
#include "code/include/game.h"
#include "code/include/hall_of_fame.h"
#include "code/include/menu.h"
#include "ui_score.h"

extern Menu* menu;
extern Game* game;
extern bool pause;

score::score(QWidget* parent)
    : QMainWindow(parent)
    , ui(new Ui::score)
{
    ui->setupUi(this);
}

score::~score() { delete ui; }

// set labels and window title based on did player won or lost the game
void score::setParametarForGameWinOrLose(int param)
{

    ui->label->setText(QString::number(game->player_score->getScore()));
    ui->label->setStyleSheet("QLabel { color: white; }");

    ui->label->move(menu->menu_width / 2 - ui->label->width(),
        menu->menu_height / 2 - ui->label->height() / 2);
    ui->lineEdit->move(menu->menu_width / 2 - ui->lineEdit->width() / 2 + 100,
        menu->menu_height / 2 + ui->typeName->height() / 2 + ui->lineEdit->height() / 2);

    ui->yourScore->move(menu->menu_width / 2 - ui->yourScore->width() / 2 - ui->saveResultButton->width(),
        menu->menu_height / 2 - ui->yourScore->height() / 2);
    ui->typeName->move(menu->menu_width / 2 - ui->yourScore->width() / 2 - ui->saveResultButton->width(),
        menu->menu_height / 2 + ui->typeName->height() / 2);

    // Position of buttons
    ui->saveResultButton->move(
        menu->menu_width / 2 - ui->saveResultButton->width() / 2 - 100,
        menu->menu_height / 2 + ui->typeName->height() / 2 + 70);
    ui->playAgainButton->move(
        menu->menu_width / 2 - ui->saveResultButton->width() / 2 + 80,
        menu->menu_height / 2 + ui->typeName->height() / 2 + 70);
    ui->goToMenuButton->move(
        menu->menu_width / 2 - ui->saveResultButton->width() / 2 - 100,
        menu->menu_height / 2 - ui->saveResultButton->height() / 2 + 210);
    ui->exitButton->move(
        menu->menu_width / 2 - ui->saveResultButton->width() / 2 + 80,
        menu->menu_height / 2 - ui->saveResultButton->height() / 2 + 210);

    if (param == 1) {

        ui->centralWidget->setStyleSheet(
            "background-image: "
            "url(:images/won_background);background-position: "
            "center; ");

        this->setWindowTitle("Congratulations! You saved Christmas!");
    } else {
        ui->centralWidget->setStyleSheet(
            "background-image: url(:images/lost);background-position: "
            "center; ");

        this->setWindowTitle("Game lost! Good luck next time!");
    }
}

// save name-score and go to Hof
void score::on_saveResultButton_clicked()
{
    HallOfFame* hof = new HallOfFame();

    QString name = ui->lineEdit->text().split(" ").at(0);
    int points = game->player_score->getScore();
    hof->addPlayer(name, points, game->lm->level_counter);
    hof->printListToHallOfFame();

    // disable because there is no game to continue
    menu->disable_continueButton();
    hof->showMaximized();
    this->hide();
}

// go to the menu
void score::on_goToMenuButton_clicked()
{
    menu->disable_continueButton();
    menu->showMaximized();
    this->close();
}

// start the game
void score::on_playAgainButton_clicked()
{
    pause = false;
    // creates new game
    game->resetGame();
    game->showMaximized();
    game->lm->manager();
    this->close();
}

// exit from app
void score::on_exitButton_clicked() { exit(0); }

void score::paintEvent(QPaintEvent*)
{
    QStyleOption opt;
    opt.init(this);
    QPainter p(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
}
