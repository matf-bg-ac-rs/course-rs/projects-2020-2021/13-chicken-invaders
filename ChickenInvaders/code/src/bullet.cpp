#include "code/include/bullet.h"
#include "code/include/boss.h"
#include "code/include/chicken.h"
#include "code/include/game.h"
#include "code/include/meteor.h"

extern bool pause;
extern Game* game;

Bullet::Bullet(int alfa, float scale)
{
    setPixmap(QPixmap(":/images/candy"));

    angle = alfa;
    setRotation(alfa);
    setScale(scale);
    if (scale == 1) {
        strength = 1;
    } else {
        strength = 2;
    }
}

void Bullet::destroy()
{
    scene()->removeItem(this);
    game->cleaner->cleaner->append(this);
}

// function connect to timer
void Bullet::advance(int a)
{
    (void)a;

    // bullet collides with enemy
    QList<QGraphicsItem*> colliding_items = collidingItems();
    int number_of_collision = colliding_items.size();
    for (int i = 0, n = number_of_collision; i < n; i++) {
        auto item = colliding_items[i];
        if (typeid(*(item)) == typeid(Chicken)) {
            // detected chiken
            // increase player score
            game->player_score->increseScore(
                game->player_score->chicken_points);

            // cast
            Chicken* chicken = qgraphicsitem_cast<Chicken*>(colliding_items[i]);
            // decrease chicken hp
            chicken->decreaseHp(strength);

            destroy();
            return;
        } else if (typeid(*(item)) == typeid(Boss)) {
            // detected Boss
            // increase player score
            game->player_score->increseScore(game->player_score->boss_points);

            // cast
            Boss* boss = qgraphicsitem_cast<Boss*>(colliding_items[i]);
            // decrease boss hp
            boss->decreaseHp(strength);

            destroy();
            return;
        } else if (typeid(*(item)) == typeid(Meteor)) {
            Meteor* meteor = qgraphicsitem_cast<Meteor*>(colliding_items[i]);

            meteor->decreaseHp(strength);

            destroy();
            return;
        }
    }

    // bullet didn't hit object
    if (y() < -BULLET_HEIGHT || x() < -BULLET_WIDTH || x() > game->game_width) {
        // if bullet went off screen destroy bullet
        destroy();
    } else if (!pause) {
        // else move object uphead
        int vec = angle / 5;
        setPos(x() + vec, y() - speed + abs(vec) / 2);
    }
}
