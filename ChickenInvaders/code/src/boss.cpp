#include "code/include/boss.h"
#include "code/include/bullet.h"
#include "code/include/droppable_items.h"
#include "code/include/game.h"
#include "code/include/level_manager.h"

extern Game* game;
extern bool pause;
extern Menu* menu;

Boss::Boss(int x, int y)
{
    setPos(x, y);
    // drew the boss
    setPixmap(QPixmap(":/images/boss1"));
    // init is 50 and after every 10 lvl hp increase 25
    hp = 50 + 25 * game->lm->level_counter / 10;
    special_hp = hp / 2;
    shot_counter = 0;
    radius = 50;
    entrance_complete = false;
}

void Boss::destroy()
{
    scene()->removeItem(this);
    game->cleaner->cleaner->append(this);
}

// death of boss
void Boss::haveToDie()
{
    if (hp <= 0) {
        dieSound();
        game->lm->waveSub();
        game->player_score->increseScore(game->player_score->boss_points);

        drop_gift();
        drop_food();
        destroy();
    }
}

// boss death sound
void Boss::dieSound()
{
    if (game->bossSound->state() == QMediaPlayer::PlayingState) {
        game->bossSound->setPosition(0);
    }
    game->bossSound->play();
}

// function connect to timer
void Boss::advance(int a)
{
    (void)a;
    flapWings();
    if (entrance_complete) {
        movement();
    } else {
        entrance_movement();
    }
    if (hp > 0) {
        shoot();
    }
}

// make eggs
void Boss::spawnBossEgg(int x, int y, int v_x, int v_y)
{
    BossEgg* BBED = new BossEgg(x, y, v_x, v_y);
    scene()->addItem(BBED);
}

// boss weapon, to shot 8 eggs in circle
void Boss::shootEggsInCircles()
{
    spawnBossEgg(x() + BOSS_WIDTH, y() + BOSS_HEIGHT / 2, 4, 0);
    spawnBossEgg(x() + BOSS_WIDTH * 3 / 4, y() + BOSS_HEIGHT * 3 / 4, 2, 2);

    spawnBossEgg(x() + BOSS_WIDTH / 2, y() + BOSS_HEIGHT, 0, 4);
    spawnBossEgg(x() + BOSS_WIDTH / 4, y() + BOSS_HEIGHT * 3 / 4, -2, 2);

    spawnBossEgg(x(), y() + BOSS_HEIGHT / 2, -4, 0);
    spawnBossEgg(x() + BOSS_WIDTH / 4, y() + BOSS_HEIGHT / 4, -2, -2);

    spawnBossEgg(x() + BOSS_WIDTH / 2, y(), 0, -4);
    spawnBossEgg(x() + BOSS_WIDTH * 3 / 4, y() + BOSS_HEIGHT / 4, 2, -2);
}

// special boss weapon, to shot 8 eggs in circle, diff to std circle
void Boss::shootEggsSpecial()
{
    spawnBossEgg(x() + BOSS_WIDTH * 2 / 3, y() + BOSS_HEIGHT / 2, 3, 1);
    spawnBossEgg(x() + BOSS_WIDTH / 2, y() + BOSS_HEIGHT * 2 / 3, 1, 3);

    spawnBossEgg(x() + BOSS_WIDTH / 2, y() + BOSS_HEIGHT * 2 / 3, -1, 3);
    spawnBossEgg(x() + BOSS_WIDTH / 3, y() + BOSS_HEIGHT / 2, -3, 1);

    spawnBossEgg(x() + BOSS_WIDTH / 3, y() + BOSS_HEIGHT / 2, -3, -1);
    spawnBossEgg(x() + BOSS_WIDTH / 2, y() + BOSS_HEIGHT / 3, -1, -3);

    spawnBossEgg(x() + BOSS_WIDTH * 2 / 3, y() + BOSS_HEIGHT / 3, 1, -3);
    spawnBossEgg(x() + BOSS_WIDTH * 2 / 3, y() + BOSS_HEIGHT / 2, 3, -1);
}

// control movement of boss
void Boss::movement()
{
    const int buffer = 30;
    if (!pause) {
        if (x() > game->game_width - BOSS_WIDTH - buffer) {
            direction_x = -1;
        }
        if (x() < buffer) {
            direction_x = 1;
        }
        if (y() > game->game_height - BOSS_HEIGHT - buffer) {
            direction_y = -1;
        }
        if (y() < buffer) {
            direction_y = 1;
        }
        if (speed_movement < 2) {
            speed_movement += 0.002;
        }
        setPos(x() + speed_movement * direction_x,
            y() + speed_movement * direction_y);
    }
}

// movement for entrering boss
void Boss::entrance_movement()
{
    if (!pause) {
        setPos(x(), y() + 4 * speed_movement);
        if (x() >= game->game_width / 2 - BOSS_WIDTH / 2 && y() >= BOSS_HEIGHT / 2) {
            entrance_complete = true;
        }
    }
}

// when boss is hit decrease hp
void Boss::decreaseHp(int x)
{
    hp -= x;
    haveToDie();
}

// effect of rocket to boss: decresase 20 hp
void Boss::rocketHit() { decreaseHp(20); }

// shoot once per second (when shot counter hits 6)
void Boss::shoot()
{
    if (!pause) {
        int randShoot = (std::rand() % 10000);
        if (randShoot <= 166) {
            shootEggsInCircles();
        }
        if (randShoot <= 44) {
            shootEggsSpecial();
        }
        if (special_hp >= hp) {
            shootEggsInCircles();
            shootEggsSpecial();
            special_hp = special_hp < 10 ? 0 : special_hp / 2;
        }
    }
}

// drop food
void Boss::drop_food()
{
    int level = game->lm->level_counter;
    int randFood = (std::rand() % (10 + level / 2)) + 6;
    for (int i = 0; i <= randFood; i++) {
        int randN = (std::rand() % 100);
        Food* food = new Food(x(), y());
        food->setPos(x() + 2 * randN, y() + 20 + 2 * i);
        scene()->addItem(food);
    }
}

// drop gift
void Boss::drop_gift()
{
    int randX = (std::rand() % 100);
    createGift(x() + randX, y()); // siguran jedan poklon
    int giftChance = (std::rand() % 100);
    if (giftChance <= 40) { // 40% sansa za 2 poklona
        randX = (std::rand() % 100);
        createGift(x() + randX, y());
    }
    if (giftChance <= 20) { // 20% sansa za 3 poklona
        randX = (std::rand() % 100);
        createGift(x() + randX, y());
    }
}

// creat gift
void Boss::createGift(int x, int y)
{
    Gift* gift = new Gift(x, y);
    scene()->addItem(gift);
}

// animation of "flying"
void Boss::flapWings()
{
    flap_wing >= wing_speed ? flap_wing = 0 : flap_wing++;
    if (flap_wing < wing_speed / 2) {
        setPixmap(boss_1);
    } else {
        setPixmap(boss_2);
    }
}
