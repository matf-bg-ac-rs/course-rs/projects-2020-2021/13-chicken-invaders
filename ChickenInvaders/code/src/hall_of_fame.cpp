#include "code/include/hall_of_fame.h"
#include "code/include/game.h"
#include "code/include/menu.h"
#include "ui_hall_of_fame.h"

extern Menu* menu;
extern Game* game;

HallOfFame::HallOfFame(QWidget* parent)
    : QWidget(parent)
    , ui(new Ui::HallOfFame)
{
    ui->setupUi(this);
    QPixmap bkgnd(":/images/hof");
    QPalette palette;
    palette.setBrush(QPalette::Background, bkgnd);
    this->setPalette(palette);
}

HallOfFame::~HallOfFame() { delete ui; }

void HallOfFame::paintEvent(QPaintEvent*)
{
    QStyleOption opt;
    opt.init(this);
    QPainter p(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
}

// goes to menu
void HallOfFame::on_goToMenuButton_clicked()
{
    menu->showMaximized();
    this->close();
}

struct QPairScoreComparer {
    template <typename T1, typename T2, typename T3>
    bool operator()(const std::tuple<T1, T2, T3>& a,
        const std::tuple<T1, T2, T3>& b)
    {
        return std::get<1>(a) > std::get<1>(b);
    }
};

// adding name-score to memory
void HallOfFame::addPlayer(QString& name, int score, int level)
{
    playerList.clear();

    readFromFileAndInsertIntoList();

    sortPlayersByScore();

    insertPlayerIntoList(name, score, level);

    printPlayersIntoFile();
}

// reading players from file and inserting into list
void HallOfFame::readFromFileAndInsertIntoList()
{
    QFile players("../players.txt");
    players.open(QIODevice::ReadWrite | QIODevice::Text);

    QTextStream stream(&players);

    while (!stream.atEnd()) {
        QString line = stream.readLine();

        QStringList lineList = line.split(" ");
        QString name = lineList.at(0);
        int score = lineList.at(1).toInt();
        int level = lineList.at(2).toInt();

        playerList.append(std::make_tuple(name, score, level));
    }

    players.close();
}

// sorting list of players
void HallOfFame::sortPlayersByScore()
{
    std::sort(playerList.begin(), playerList.end(), QPairScoreComparer());
}

// inserting new player to list
void HallOfFame::insertPlayerIntoList(QString name, int score, int level)
{
    int position = -1;
    for (int i = 0; i < playerList.size(); i++) {
        std::tuple<QString, int, int> player = playerList.at(i);
        int score1 = std::get<1>(player);

        if (score > score1) {
            position = i;
            break;
        }
    }

    if (position != -1) {
        for (int i = playerList.size() - 1; i >= 0; i--) {
            if (i > position) {
                playerList[i] = playerList.at(i - 1);
            } else if (i == position) {
                std::tuple<QString, int, int> currentPlayer = std::make_tuple(name, score, level);
                playerList[i] = currentPlayer;
            }
        }
    }
}

// printing players from list to file
void HallOfFame::printPlayersIntoFile()
{
    QFile players("../players.txt");
    players.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate);

    QTextStream stream(&players);

    for (int i = 0; i < playerList.size(); i++) {
        std::tuple<QString, int, int> player = playerList.at(i);
        stream << std::get<0>(player) << " " << std::get<1>(player) << " "
               << std::get<2>(player) << '\n';
    }

    players.close();
}

// printing list of players into Hall of Fame
void HallOfFame::printListToHallOfFame()
{
    playerList.clear();
    readFromFileAndInsertIntoList();
    sortPlayersByScore();

    QString names;
    QString scores;
    QString levels;
    QString positions;

    for (int i = 0; i < playerList.size(); i++) {
        std::tuple<QString, int, int> player = playerList.at(i);
        QString position = QString::number(i + 1) + ". ";
        QString name = std::get<0>(player);
        QString score = QString::number(std::get<1>(player));
        QString level = QString::number(std::get<2>(player));

        positions += QString("%1").arg(position, 5, QChar(' ')) + "\n";
        names += QString("%1").arg(name, 15, QChar(' ')) + "\n";
        scores += QString("%1").arg(score, 10, QChar(' ')) + "\n";
        levels += QString("%1").arg(level, 10, QChar(' ')) + "\n";
    }

    ui->label_4->setText(positions);
    ui->label_4->setStyleSheet(
        "QLabel { color: white; font-size: 18px; font:bolid;}");

    ui->label->setText(names);
    ui->label->setStyleSheet(
        "QLabel { color: white; font-size: 18px; font:bolid;}");

    ui->label_2->setText(scores);
    ui->label_2->setStyleSheet(
        "QLabel { color: white; font-size: 18px; font:bolid;}");

    ui->label_3->setText(levels);
    ui->label_3->setStyleSheet(
        "QLabel { color: white; font-size: 18px; font:bolid; }");

    ui->player->move(menu->menu_width / 2 - ui->score->width() / 2 - ui->player->width() - 10,
        menu->menu_height / 2 - ui->score->height() / 2 - 2 * ui->label_2->height() / 4);
    ui->label->move(menu->menu_width / 2 - ui->label_2->width() / 2 - ui->player->width(),
        menu->menu_height / 2 - ui->label_2->height() / 2 + ui->label_2->height() / 8);
    ui->label_4->move(menu->menu_width / 2 - ui->label_2->width() / 2 - ui->player->width() - 20,
        menu->menu_height / 2 - ui->label_2->height() / 2 + ui->label_2->height() / 8);

    ui->score->move(menu->menu_width / 2 - ui->player->width() / 2 + 20,
        menu->menu_height / 2 - ui->score->height() / 2 - 2 * ui->label_2->height() / 4);
    ui->label_2->move(menu->menu_width / 2 - ui->label_2->width() / 2 + 20,
        menu->menu_height / 2 - ui->label_2->height() / 2 + ui->label_2->height() / 8);

    ui->level->move(menu->menu_width / 2 - ui->score->width() / 2 + ui->player->width() + 20,
        menu->menu_height / 2 - ui->score->height() / 2 - 2 * ui->label_2->height() / 4);
    ui->label_3->move(menu->menu_width / 2 - ui->label_2->width() / 2 + ui->player->width() + 20,
        menu->menu_height / 2 - ui->label_2->height() / 2 + ui->label_2->height() / 8);

    ui->goToMenuButton->move(menu->menu_width / 2 - ui->player->width() / 2,
        menu->menu_height / 2 + ui->label_2->height() - 10);
}
