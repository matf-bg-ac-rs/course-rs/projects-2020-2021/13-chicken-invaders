#include "code/include/player.h"
#include "code/include/boss.h"
#include "code/include/bullet.h"
#include "code/include/chicken.h"
#include "code/include/droppable_items.h"
#include "code/include/game.h"
#include "code/include/meteor.h"
#include "code/include/pause.h"
#include "code/include/rocket.h"

#define SHIELD_DURATION (2000)
#define PLAYER_HIT_BOSS (10)

extern QMediaPlayer* music_player;
extern Menu* menu;
extern Game* game;
extern bool pause;

Player::Player()
{
    weapon_lvl = 0;
    health = 3;
    rocket_count = 1;
    shooting = false;
    rocket_cooldown = false;
    setPixmap(player_image);

    cooldown_timer->setSingleShot(true);
    connect(cooldown_timer, SIGNAL(timeout()), this, SLOT(resetCooldownFire()));
    shield_timer->setSingleShot(true);
    connect(shield_timer, SIGNAL(timeout()), this, SLOT(removeShield()));
    rocket_cooldown_timer->setSingleShot(true);
    connect(rocket_cooldown_timer, SIGNAL(timeout()), this,
        SLOT(resetRocketCooldownFire()));
}

Player::~Player()
{
    scene()->removeItem(this);
    delete this;
}

// reset value for new game
void Player::resetPlayer()
{
    this->setPos(game->game_width / 2 - PLAYER_WIDTH / 2,
        game->game_height - PLAYER_HEIGHT * 1.5);
    fire_cooldown = false;
    rocket_cooldown = false;
    shooting = false;
    cooldown_timer->stop();
    haveShield = false;
    shield_timer->stop();
    setPixmap(player_image);
    weapon_lvl = 0;
    for (; health > 0; health--) {
        scene()->removeItem(game->health_bar.last());
        game->health_bar.removeLast();
    }
    for (; health < 3;) {
        addPlayerHealth();
    }

    for (; rocket_count > 0;) {
        reducePlayerRockets();
    }
    addPlayerRockets();

    alive = true;
}

// increase one life and add image representation
void Player::addPlayerHealth()
{
    const int max_hp = 10;
    if (health < max_hp) {
        health++;
        game->health_bar.append(new PlayerHealth);
        game->health_bar.last()->setPos(
            game->game_width - (game->health_bar.count()) * (HP_SIZE + 5), 5);
        scene()->addItem(game->health_bar.last());
    }
}

// increase one rocket and add image representation
void Player::addPlayerRockets()
{
    const int max_rocket_count = 10;
    if (rocket_count < max_rocket_count) {
        rocket_count++;
        game->rocket_bar.append(new RocketCountIndikator);
        game->rocket_bar.last()->setPos(game->game_width - (game->rocket_bar.count()) * (RC_INDICATOR_WIDTH + 10),
            TOPSPACEING - 5);
        scene()->addItem(game->rocket_bar.last());
    }
}

// reduce one life, weapon level and give player protection
// if it is the last one, declare death of player, go to score
void Player::reducePlayerHealth()
{
    if (alive) {
        // giving player shield when he loses life
        if (!haveShield) {
            haveShield = true;
            setPos(x() - 8, y() - 2);
            setPixmap(player_shield_image);

            shield_timer->start(SHIELD_DURATION);
        }

        health--;
        scene()->removeItem(game->health_bar.last());
        game->health_bar.removeLast();

        // punish player with lower weapon
        if (weapon_lvl >= 3) {
            weapon_lvl -= 3;
        } else if (weapon_lvl > 0) {
            weapon_lvl--;
        }

        // that was last live => call death
        if (health == 0) {
            playerDeath();
        }
    }
}

// death of the player => end game => go to score window
void Player::playerDeath()
{
    alive = false;
    pause = true;

    QMediaPlaylist* bg_music_list = new QMediaPlaylist();
    bg_music_list->addMedia(QUrl("qrc:/sounds/bg_sound2"));
    bg_music_list->setPlaybackMode(QMediaPlaylist::Loop);

    music_player->stop();
    music_player->setPlaylist(bg_music_list);
    music_player->play();

    if (game->lm->level_counter == 101) { // player won
        menu->show_score(1);
    } else { // player lost
        menu->show_score(0);
    }

    game->hide();
}

// getter for weapon lvl
int Player::getPlayerWeaponLevel() { return weapon_lvl; }

// player cant spam primary fire
void Player::resetCooldownFire() { fire_cooldown = false; }

// player cant spam rockets
void Player::resetRocketCooldownFire() { rocket_cooldown = false; }

// remove shiled(protection)
void Player::removeShield()
{
    setPixmap(player_image);
    setPos(x() + 9, y() + 1);
    haveShield = false;

    if (!shield_timer->isActive()) {
        return;
    }
}

// geter for lifes
int Player::getPlayerHealth() { return health; }

// geter for number of rocket
int Player::getPlayerRockets() { return rocket_count; }

// increase player score and if it over 150 get him one rocket
void Player::EatFood(int n)
{
    game->player_score->addFoodEaten(n);
    int food_eaten_tmp = (game->player_score->food_eaten);
    if (n == 10) {
        game->player_score->increseScore(6 * game->player_score->food_points);
    } else {
        game->player_score->increseScore(game->player_score->food_points);
    }
    if (food_eaten_tmp >= num_food_new_rocket) {
        addPlayerRockets();
        game->player_score->food_eaten -= num_food_new_rocket;
    }
}

// function connect to timer
void Player::advance(int a)
{
    (void)a;

    // move player
    movement();

    if (!pause && shooting) {
        fireWeapon();
    }

    // colider for player and every object on screen
    QList<QGraphicsItem*> colliding_items = collidingItems();
    for (int i = 0, n = colliding_items.size(); i < n; ++i) {
        auto item = colliding_items[i];
        if (typeid(*(item)) == typeid(Egg)) {
            if (!haveShield)
                reducePlayerHealth();

            Egg* egg = qgraphicsitem_cast<Egg*>(colliding_items[i]);
            egg->destroy();

            return;
        } else if (typeid(*(item)) == typeid(Gift)) {

            // increse score
            game->player_score->increseScore(game->player_score->gift_points);

            // get power up, max = 20 + buffer(2)
            const int max_weapon_level = 22;
            if (weapon_lvl < max_weapon_level) {
                weapon_lvl++;
            }

            Gift* gift = qgraphicsitem_cast<Gift*>(colliding_items[i]);
            gift->destroy();

            return;
        } else if (typeid(*(item)) == typeid(Chicken)) {
            if (!haveShield) {
                reducePlayerHealth();

                game->lm->waveSub();
                Chicken* chicken = qgraphicsitem_cast<Chicken*>(colliding_items[i]);
                chicken->destroy();
            }
            return;
        } else if (typeid(*(item)) == typeid(Boss)) {
            if (!haveShield) {
                reducePlayerHealth();

                Boss* boss = qgraphicsitem_cast<Boss*>(colliding_items[i]);
                boss->decreaseHp(PLAYER_HIT_BOSS);
            }
            return;
        } else if (typeid(*(item)) == typeid(Food)) {
            Food* food = qgraphicsitem_cast<Food*>(colliding_items[i]);
            if (food->roast) {
                EatFood(10);
            } else {
                EatFood(1);
            }
            food->destroy();
            return;
        } else if (typeid(*(item)) == typeid(Meteor)) {

            if (!haveShield) {
                reducePlayerHealth();

                Meteor* meteorite = qgraphicsitem_cast<Meteor*>(colliding_items[i]);
                game->lm->waveSub();
                meteorite->destroy();
            }
            return;
        } else if (typeid(*(item)) == typeid(BossEgg)) {
            if (!haveShield)
                reducePlayerHealth();

            BossEgg* begg = qgraphicsitem_cast<BossEgg*>(colliding_items[i]);
            begg->destroy();

            return;
        }
    }
}

// player movement
void Player::movement()
{
    if (move_up) {
        if (y() - speed >= 0) {
            if (move_left ^ move_right)
                setPos(x(), y() - speed + 1);
            else
                setPos(x(), y() - speed);
        } else {
            move_up = false;
        }
    }

    if (move_down) {
        if (y() + speed <= game->game_height - PLAYER_HEIGHT) {
            if (move_left ^ move_right)
                setPos(x(), y() + speed - 1);
            else
                setPos(x(), y() + speed);
        } else {
            move_down = false;
        }
    }
    if (move_left) {
        if (x() - speed >= 0) {
            if (move_up ^ move_down)
                setPos(x() - speed + 1, y());
            else
                setPos(x() - speed, y());
        } else {
            move_left = false;
        }
    }
    if (move_right) {
        if (x() + speed <= game->game_width - PLAYER_WIDTH) {
            if (move_up ^ move_down)
                setPos(x() + speed - 1, y());
            else
                setPos(x() + speed, y());
        } else {
            move_right = false;
        }
    }
}

// Function that checks for Key press envent
// Depending on the key pressed the player will move or fire
void Player::keyPressEvent(QKeyEvent* event)
{
    if ((event->key() == Qt::Key_A || event->key() == Qt::Key_Left) && pos().x() > 0 && !pause) {
        move_left = true;
    }
    if ((event->key() == Qt::Key_D || event->key() == Qt::Key_Right) && (pos().x() + PLAYER_WIDTH < scene()->width()) && !pause) {
        move_right = true;
    }
    if ((event->key() == Qt::Key_W || event->key() == Qt::Key_Up) && pos().y() > 0 && !pause) {
        move_up = true;
    }
    if ((event->key() == Qt::Key_S || event->key() == Qt::Key_Down) && (pos().y() + PLAYER_HEIGHT < scene()->height()) && !pause) {
        move_down = true;
    }
    if (event->key() == Qt::Key_Space && !fire_cooldown && !pause) {
        // u zavisnosti od scora pravi se bullet
        shooting = true;
        fireWeapon();

        if (game->laserSound->state() == QMediaPlayer::PlayingState) {
            game->laserSound->setPosition(0);
        }
        game->laserSound->play();
    }
    if ((event->key() == Qt::Key_R || event->key() == Qt::Key_Shift) && !pause && !rocket_cooldown) {
        FireRocket();
        rocket_cooldown = true;
        rocket_cooldown_timer->start(rocket_cooldown_time);
    }
    if (event->key() == Qt::Key_Escape) {
        pause = true;
        shooting = false;
        move_left = false;
        move_right = false;
        move_up = false;
        move_down = false;
        Pause* pause_menu = new Pause();
        pause_menu->showMaximized();
        game->hide();
    }
    // cheats for dev testing, remove or put in comment before release
    if (event->key() == Qt::Key_L) {
        // lose life
        reducePlayerHealth();
    }
    if (event->key() == Qt::Key_K) {
        // kill everthing on screen
        RocketShot();
    }
    if (event->key() == Qt::Key_J) {
        // get new rocket
        addPlayerRockets();
    }
    if (event->key() == Qt::Key_H) {
        // get new life
        addPlayerHealth();
    }
    if (event->key() == Qt::Key_Q) {
        // upgrade weapon
        weapon_lvl++;
    }
    if (event->key() == Qt::Key_N) {
        // next level
        goToNextLevel();
    }
}

// Function that checks for Key release envent
// Depending on the key release the player will stop moving
void Player::keyReleaseEvent(QKeyEvent* event)
{
    if (event->key() == Qt::Key_A || event->key() == Qt::Key_Left) {
        move_left = false;
    }
    if (event->key() == Qt::Key_D || event->key() == Qt::Key_Right) {
        move_right = false;
    }
    if (event->key() == Qt::Key_W || event->key() == Qt::Key_Up) {
        move_up = false;
    }
    if (event->key() == Qt::Key_S || event->key() == Qt::Key_Down) {
        move_down = false;
    }
    if (event->key() == Qt::Key_Space) {
        shooting = false;
    }
}

// reduce num. of rocket
void Player::reducePlayerRockets()
{
    rocket_count--;
    scene()->removeItem(game->rocket_bar.last());
    game->rocket_bar.removeLast();
}

// rocket launch
void Player::FireRocket()
{
    if (rocket_count > 0) {
        reducePlayerRockets();
        Rocket* rocket = new Rocket();
        rocket->setPos(x() + PLAYER_WIDTH * 2 / 5 - 8, y() - PLAYER_HEIGHT / 2);
        rocket->stepY = (game->game_height / 2 - y()) / rocket->stepNumber;
        rocket->stepX = (game->game_width / 2 - x()) / rocket->stepNumber;
        scene()->addItem(rocket);
    }
}

// effect of rocket
void Player::RocketShot()
{
    foreach (QGraphicsItem* item, game->scene()->items()) {
        if (typeid(*item) == typeid(Chicken)) {
            Chicken* chicken = qgraphicsitem_cast<Chicken*>(item);
            chicken->destroy();
            game->lm->waveSub();
        } else if (typeid(*item) == typeid(Egg)) {
            Egg* egg = qgraphicsitem_cast<Egg*>(item);
            egg->destroy();
        } else if (typeid(*item) == typeid(Boss)) {
            Boss* boss = qgraphicsitem_cast<Boss*>(item);
            boss->rocketHit();
        } else if (typeid(*item) == typeid(Meteor)) {
            Meteor* meteor = qgraphicsitem_cast<Meteor*>(item);
            meteor->destroy();
            game->lm->waveSub();
        } else if (typeid(*item) == typeid(BossEgg)) {
            BossEgg* begg = qgraphicsitem_cast<BossEgg*>(item);
            begg->destroy();
        }
    }
}

// choose tip of weapon based on weapon_lvl
void Player::fireWeapon()
{
    if (!fire_cooldown) {
        // Bullet is drawn depending on weapon level
        const int player_mid_w = PLAYER_WIDTH / 2;
        const int player_mid_h = PLAYER_HEIGHT / 2;
        const int b_angle = 5;
        if (weapon_lvl == 0) {
            spawnBullet(x() + player_mid_w - BULLET_WIDTH / 2,
                y() - player_mid_h, 0, 1);
        } else if (weapon_lvl == 1) {
            spawnBullet(x() + player_mid_w - BULLET_WIDTH - 11,
                y() - player_mid_h, 0, 1);
            spawnBullet(x() + player_mid_w + BULLET_WIDTH - 2,
                y() - player_mid_h, 0, 1);
        } else if (weapon_lvl == 2) {
            spawnBullet(x() + player_mid_w - BULLET_WIDTH - 8,
                y() - player_mid_h, 0, 1);
            spawnBullet(x() + player_mid_w - BULLET_WIDTH / 2,
                y() - player_mid_h - 15, 0, 1);
            spawnBullet(x() + player_mid_w + 8, y() - player_mid_h, 0, 1);
        } else if (weapon_lvl == 3) {
            spawnBullet(x() + player_mid_w - BULLET_WIDTH - 8,
                y() - player_mid_h, -5, 1);
            spawnBullet(x() + player_mid_w - BULLET_WIDTH / 2,
                y() - player_mid_h - 15, 0, 1);
            spawnBullet(x() + player_mid_w + 8, y() - player_mid_h, 5, 1);
        } else if (weapon_lvl == 4) {
            spawnBullet(x() + player_mid_w + BULLET_WIDTH + 12,
                y() - player_mid_h, b_angle, 1);
            spawnBullet(x() + player_mid_w - BULLET_WIDTH - 4,
                y() - player_mid_h - 15, 0, 1);
            spawnBullet(x() + player_mid_w + 4, y() - player_mid_h - 15, 0, 1);
            spawnBullet(x() + player_mid_w - 2 * BULLET_WIDTH - 12,
                y() - player_mid_h, -b_angle, 1);
        } else if (weapon_lvl == 5) {
            spawnBullet(x() + player_mid_w - 2 * BULLET_WIDTH - 14,
                y() - player_mid_h, -b_angle * 2, 1);
            spawnBullet(x() + player_mid_w - BULLET_WIDTH - 8,
                y() - player_mid_h - 8, -b_angle, 1);
            spawnBullet(x() + player_mid_w - BULLET_WIDTH / 2,
                y() - player_mid_h - 15, 0, 1);
            spawnBullet(x() + player_mid_w + 8, y() - player_mid_h - 8, b_angle,
                1);
            spawnBullet(x() + player_mid_w + BULLET_WIDTH + 14,
                y() - player_mid_h, b_angle * 2, 1);
        } else if (weapon_lvl == 6) {
            spawnBullet(x() + player_mid_w - 2 * BULLET_WIDTH - 14,
                y() - player_mid_h, -b_angle * 2, 1);
            spawnBullet(x() + player_mid_w - BULLET_WIDTH - 8,
                y() - player_mid_h - 8, -b_angle, 1);
            spawnBullet(x() + player_mid_w - (BULLET_WIDTH * BIG_BULLET) / 2,
                y() - player_mid_h - 15, 0, BIG_BULLET);
            spawnBullet(x() + player_mid_w + 8, y() - player_mid_h - 8, b_angle,
                1);
            spawnBullet(x() + player_mid_w + BULLET_WIDTH + 14,
                y() - player_mid_h, b_angle * 2, 1);
        } else if (weapon_lvl == 7) {
            spawnBullet(x() + player_mid_w - 3 * BULLET_WIDTH - 14,
                y() - player_mid_h, -b_angle * 2, 1);
            spawnBullet(x() + player_mid_w - 2 * BULLET_WIDTH - 10,
                y() - player_mid_h - 7, -b_angle, 1);
            spawnBullet(x() + player_mid_w - BULLET_WIDTH - 4,
                y() - player_mid_h - 15, 0, 1);
            spawnBullet(x() + player_mid_w + 4, y() - player_mid_h - 15, 0, 1);
            spawnBullet(x() + player_mid_w + BULLET_WIDTH + 10,
                y() - player_mid_h - 7, b_angle, 1);
            spawnBullet(x() + player_mid_w + 2 * BULLET_WIDTH + 14,
                y() - player_mid_h, b_angle * 2, 1);
        } else if (weapon_lvl == 8) {
            spawnBullet(x() + player_mid_w - 3 * BULLET_WIDTH - 14,
                y() - player_mid_h, -b_angle * 2, 1);
            spawnBullet(x() + player_mid_w - 2 * BULLET_WIDTH - 10,
                y() - player_mid_h - 7, -b_angle, 1);
            spawnBullet(x() + player_mid_w - (BULLET_WIDTH * BIG_BULLET) - 4,
                y() - player_mid_h - 15, 0, BIG_BULLET);
            spawnBullet(x() + player_mid_w + 4, y() - player_mid_h - 15, 0,
                BIG_BULLET);
            spawnBullet(x() + player_mid_w + BULLET_WIDTH + 10,
                y() - player_mid_h - 7, b_angle, 1);
            spawnBullet(x() + player_mid_w + 2 * BULLET_WIDTH + 14,
                y() - player_mid_h, b_angle * 2, 1);
        } else if (weapon_lvl == 9) {
            spawnBullet(x() + player_mid_w - 2 * (BULLET_WIDTH * BIG_BULLET) - 14,
                y() - player_mid_h - 7, -b_angle, BIG_BULLET);
            spawnBullet(x() + player_mid_w - (BULLET_WIDTH * BIG_BULLET) - 4,
                y() - player_mid_h - 15, 0, BIG_BULLET);
            spawnBullet(x() + player_mid_w + 4, y() - player_mid_h - 15, 0,
                BIG_BULLET);
            spawnBullet(x() + player_mid_w + (BULLET_WIDTH * BIG_BULLET) + 14,
                y() - player_mid_h - 7, b_angle, BIG_BULLET);
        } else if (weapon_lvl <= 12) {
            spawnBullet(x() + player_mid_w - 3 * (BULLET_WIDTH * BIG_BULLET) - 8,
                y() - player_mid_h + 4, -b_angle * 2, 1);
            spawnBullet(x() + player_mid_w - 2 * (BULLET_WIDTH * BIG_BULLET) - 4,
                y() - player_mid_h - 9, -b_angle, BIG_BULLET);
            spawnBullet(x() + player_mid_w - (BULLET_WIDTH * BIG_BULLET) / 2,
                y() - player_mid_h - 15, 0, BIG_BULLET);
            spawnBullet(x() + player_mid_w + (BULLET_WIDTH * BIG_BULLET) + 4,
                y() - player_mid_h - 9, b_angle, BIG_BULLET);
            spawnBullet(x() + player_mid_w + 2 * (BULLET_WIDTH * BIG_BULLET) + 8,
                y() - player_mid_h + 4, b_angle * 2, 1);
        } else if (weapon_lvl <= 19) {
            spawnBullet(x() + player_mid_w - 3 * (BULLET_WIDTH * BIG_BULLET) - 8,
                y() - player_mid_h + 4, -b_angle * 2, BIG_BULLET);
            spawnBullet(x() + player_mid_w - 2 * (BULLET_WIDTH * BIG_BULLET) - 4,
                y() - player_mid_h - 9, -b_angle, BIG_BULLET);
            spawnBullet(x() + player_mid_w - (BULLET_WIDTH * BIG_BULLET) / 2,
                y() - player_mid_h - 15, 0, BIG_BULLET);
            spawnBullet(x() + player_mid_w + (BULLET_WIDTH * BIG_BULLET) + 4,
                y() - player_mid_h - 9, b_angle, BIG_BULLET);
            spawnBullet(x() + player_mid_w + 2 * (BULLET_WIDTH * BIG_BULLET) + 8,
                y() - player_mid_h + 4, b_angle * 2, BIG_BULLET);
        } else {
            spawnBullet(x() + player_mid_w - 4 * (BULLET_WIDTH * BIG_BULLET) - 14,
                y() - player_mid_h + 15, -b_angle * 3, 1);
            spawnBullet(x() + player_mid_w - 3 * (BULLET_WIDTH * BIG_BULLET) - 8,
                y() - player_mid_h + 4, -b_angle * 2, BIG_BULLET);
            spawnBullet(x() + player_mid_w - 2 * (BULLET_WIDTH * BIG_BULLET) - 4,
                y() - player_mid_h - 9, -b_angle, BIG_BULLET);
            spawnBullet(x() + player_mid_w - (BULLET_WIDTH * BIG_BULLET) / 2,
                y() - player_mid_h - 15, 0, BIG_BULLET);
            spawnBullet(x() + player_mid_w + (BULLET_WIDTH * BIG_BULLET) + 4,
                y() - player_mid_h - 9, b_angle, BIG_BULLET);
            spawnBullet(x() + player_mid_w + 2 * (BULLET_WIDTH * BIG_BULLET) + 8,
                y() - player_mid_h + 4, b_angle * 2, BIG_BULLET);
            spawnBullet(x() + player_mid_w + 3 * (BULLET_WIDTH * BIG_BULLET) + 14,
                y() - player_mid_h + 15, b_angle * 3, 1);
        }
        fire_cooldown = true;
        cooldown_timer->start(cooldown_time);
    }
}

// make one bullet
void Player::spawnBullet(int x, int y, int alpha, float scale)
{
    Bullet* bullet = new Bullet(alpha, scale);
    bullet->setPos(x, y);
    scene()->addItem(bullet);
}

// part of cheats, just for dev and testing
void Player::goToNextLevel()
{
    foreach (QGraphicsItem* item, game->scene()->items()) {
        if (typeid(*item) == typeid(Chicken)) {
            Chicken* chicken = qgraphicsitem_cast<Chicken*>(item);
            chicken->destroy();
        } else if (typeid(*item) == typeid(Egg)) {
            Egg* egg = qgraphicsitem_cast<Egg*>(item);
            egg->destroy();
        } else if (typeid(*item) == typeid(Boss)) {
            Boss* boss = qgraphicsitem_cast<Boss*>(item);
            boss->destroy();
        } else if (typeid(*item) == typeid(Meteor)) {
            Meteor* meteor = qgraphicsitem_cast<Meteor*>(item);
            meteor->destroy();
        } else if (typeid(*item) == typeid(Gift)) {
            Gift* gift = qgraphicsitem_cast<Gift*>(item);
            gift->destroy();
        } else if (typeid(*item) == typeid(Food)) {
            Food* food = qgraphicsitem_cast<Food*>(item);
            food->destroy();
        }
    }

    game->lm->nextLevel();
}
