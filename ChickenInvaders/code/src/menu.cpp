#include "code/include/menu.h"
#include "code/include/continue.h"
#include "code/include/credits.h"
#include "code/include/game.h"
#include "code/include/hall_of_fame.h"
#include "code/include/instructions.h"
#include "code/include/options.h"
#include "code/include/score.h"
#include "ui_menu.h"

// unique instance of game, main game object
Game* game;
extern bool pause;
QMediaPlayer* music_player;

Menu::Menu(QWidget* parent)
    : QMainWindow(parent)
    , ui(new Ui::Menu)
{
    // init game and sound
    game = NULL;
    volume = 0;

    // init
    ui->setupUi(this);

    // init menu width and height parameters

    QScreen* screen = QGuiApplication::primaryScreen();
    QRect rec = screen->geometry();
    menu_height = rec.height();
    menu_width = rec.width();

    // position of center screen
    // ui->NewGameButton->move(menu_width/2-ui->NewGameButton->width()/2,
    // menu_height/2-ui->NewGameButton->height()/2);

    ui->NewGameButton->move(
        menu_width / 2 - 2 * ui->NewGameButton->width() / 2 - ui->NewGameButton->width() / 20,
        menu_height / 2 - 3 * ui->NewGameButton->height() / 2);
    ui->ResumeButton->move(menu_width / 2 + ui->NewGameButton->width() / 20,
        menu_height / 2 - 3 * ui->ResumeButton->height() / 2);
    ui->howToPlayButton->move(
        menu_width / 2 - 2 * ui->howToPlayButton->width() / 2 - ui->howToPlayButton->width() / 20,
        menu_height / 2 - ui->howToPlayButton->height() / 4);
    ui->goToHallOfFameButton->move(
        menu_width / 2 + ui->howToPlayButton->width() / 20,
        menu_height / 2 - ui->howToPlayButton->height() / 4);
    ui->optionButton->move(menu_width / 2 - 2 * ui->NewGameButton->width() / 2 - ui->NewGameButton->width() / 20,
        menu_height / 2 + 4 * ui->howToPlayButton->height() / 4);
    ui->credits->move(menu_width / 2 + ui->NewGameButton->width() / 20,
        menu_height / 2 + 4 * ui->howToPlayButton->height() / 4);
    ui->exitButton->move(menu_width / 2 - ui->optionButton->width() / 2,
        menu_height / 2 + 5 * ui->optionButton->height() / 2);

    ui->label->move(menu_width / 2 - ui->label->width() / 2 + 10,
        menu_height / 2 - 8 * ui->NewGameButton->height() / 2);

    // at the moment there is no old game
    ui->ResumeButton->setDisabled(true);

    // Play background music
    QMediaPlaylist* bg_music_list = new QMediaPlaylist();
    bg_music_list->addMedia(QUrl("qrc:/sounds/bg_sound1"));
    bg_music_list->addMedia(QUrl("qrc:/sounds/bg_sound3"));
    bg_music_list->addMedia(QUrl("qrc:/sounds/bg_sound4"));
    bg_music_list->addMedia(QUrl("qrc:/sounds/bg_sound5"));
    bg_music_list->addMedia(QUrl("qrc:/sounds/bg_sound6"));
    bg_music_list->addMedia(QUrl("qrc:/sounds/bg_sound7"));
    bg_music_list->addMedia(QUrl("qrc:/sounds/bg_sound8"));
    bg_music_list->addMedia(QUrl("qrc:/sounds/bg_sound9"));
    bg_music_list->addMedia(QUrl("qrc:/sounds/bg_sound10"));
    bg_music_list->addMedia(QUrl("qrc:/sounds/bg_sound11"));
    bg_music_list->addMedia(QUrl("qrc:/sounds/bg_sound12"));
    bg_music_list->addMedia(QUrl("qrc:/sounds/bg_sound13"));
    bg_music_list->addMedia(QUrl("qrc:/sounds/bg_sound14"));

    bg_music_list->shuffle();
    bg_music_list->setPlaybackMode(QMediaPlaylist::Loop);

    music_player = new QMediaPlayer();
    music_player->setPlaylist(bg_music_list);
    music_player->play();
    music_player->setVolume(volume);
}

Menu::~Menu() { delete ui; }

// called from player after death of player
void Menu::show_score(int winOrLose)
{
    score* s = new score();

    if (winOrLose == 1) { // player won
        s->setParametarForGameWinOrLose(1);
    } else { // player lost
        s->setParametarForGameWinOrLose(0);
    }

    s->showMaximized();
    this->hide();
}

// disable button for continue game
void Menu::disable_continueButton()
{

    ui->ResumeButton->setStyleSheet("border-image: url(:images/continue_disabled);");
    ui->ResumeButton->setDisabled(true);
}

// enable button for continue game
void Menu::enable_continueButton()
{
    ui->ResumeButton->setStyleSheet("border-image: url(:images/continue_menu);");
    ui->ResumeButton->setDisabled(false);
}

// starting new game
void Menu::on_NewGameButton_clicked()
{
    // creates new game
    if (game == NULL) {
        pause = false;
        game = new Game();
        // enable button for continue game
        enable_continueButton();
        game->showMaximized();
        this->hide();
    } else if (!ui->ResumeButton->isEnabled()) {
        pause = false;
        enable_continueButton();
        game->resetGame();
        game->showMaximized();
        this->hide();
    } else {
        // call continue
        Continue* con = new Continue();
        con->show();
    }
}

// continue old game
void Menu::on_ResumeButton_clicked()
{
    pause = false;
    music_player->setVolume(volume);
    game->laserSound->setVolume(volume);
    game->chickenSound->setVolume(volume);
    game->showMaximized();
    game->lm->ContinueFromPause();
    this->hide();
}

// go to instruction window
void Menu::on_howToPlayButton_clicked()
{
    instructions* i = new instructions();
    i->showMaximized();
    this->hide();
}

// go to options window
void Menu::on_optionButton_clicked()
{
    if (game == NULL) {
        game = new Game();
        pause = true;
    }
    options* o = new options();
    o->showMaximized();
    this->hide();
}

// go to Hall of Fame window
void Menu::on_goToHallOfFameButton_clicked()
{
    HallOfFame* hall = new HallOfFame();

    hall->printListToHallOfFame();
    hall->showMaximized();
    this->hide();
}

// go to  credits
void Menu::on_credits_clicked()
{
    Credits* c = new Credits();
    c->showMaximized();
    this->hide();

    QTimer::singleShot(17000, c, SLOT(close()));
    QTimer::singleShot(16950, this, SLOT(showMaximized()));
}

// exit from app
void Menu::on_exitButton_clicked() { exit(0); }
