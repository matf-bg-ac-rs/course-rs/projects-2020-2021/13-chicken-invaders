#include "code/include/chicken.h"
#include "code/include/bullet.h"
#include "code/include/droppable_items.h"
#include "code/include/game.h"
#include "code/include/level_manager.h"

extern Game* game;
extern bool pause;
extern Menu* menu;

const std::vector<int> block_levels = { 1, 4, 6 };
const std::vector<int> fall_levels = { 2, 9 };
const std::vector<int> left_right_levels = { 5, 8 };

struct compare {
    int key;
    compare(int const& i)
        : key(i)
    {
    }

    bool operator()(int const& i) { return (key == i); }
};

Chicken::Chicken(int x, int y)
    : QObject()
    , QGraphicsPixmapItem()
{
    // drew the chicken
    setPixmap(chicken_1);

    setPos(x, y);

    int level = game->lm->level_counter;
    if (std::any_of(block_levels.begin(), block_levels.end(),
            compare(game->lm->level_counter % 10))) {
        speed_movement = movement_speed_init;
    } else {
        speed_movement = movement_speed_init + level / 25;
    }
    hp = 2 + level / 20;
    shoot_egg_probability = 8 + level / 15;
}

void Chicken::destroy()
{
    scene()->removeItem(this);
    game->cleaner->cleaner->append(this);
}

// makes chicken death sound
void Chicken::dieSound()
{
    if (game->chickenSound->state() == QMediaPlayer::PlayingState) {
        game->chickenSound->setPosition(0);
    }
    game->chickenSound->play();
}

// decides which of 3 forms of movement will be used depending on game level
void Chicken::movement()
{
    FlapWings();

    // Blcok, Fall,Meteor,Block, L2R, Block, Meteor, R2L, Fall, Boss
    if (std::any_of(block_levels.begin(), block_levels.end(),
            compare(game->lm->level_counter % 10))) {
        move_block();
    } else if (std::any_of(fall_levels.begin(), fall_levels.end(),
                   compare(game->lm->level_counter % 10))) {
        move_fall();
    } else if (std::any_of(left_right_levels.begin(), left_right_levels.end(),
                   compare(game->lm->level_counter % 10))) {
        move_left_right();
    }
}

// shoots an egg with 0.36% probability per second
void Chicken::shoot()
{
    if (!pause) {
        int randShoot = (std::rand() % 10000);
        if (randShoot <= shoot_egg_probability) {
            Egg* egg = new Egg(x(), y());
            egg->setPos(x(), y() + 20);
            scene()->addItem(egg);
        }
    }
}

// drops a gift with a cirtain probability of 2%
// it is called only when a chicken dies
void Chicken::drop_gifts()
{
    int randShoot = (std::rand() % 1000);
    int gift_drop_rate = 16 - game->player->getPlayerWeaponLevel() / 2;
    if (randShoot <= gift_drop_rate) {
        Gift* gift = new Gift(x(), y());
        gift->setPos(x(), y() + 20);
        scene()->addItem(gift);
    }
}

// drops food when chicken dies
void Chicken::drop_food()
{
    Food* food = new Food(x(), y());
    food->setPos(x(), y() + 20);
    scene()->addItem(food);
}

// decrese chicken HP and check if it has to die
void Chicken::decreaseHp(int x)
{
    hp -= x;
    haveToDie();
}

// check if the chicken needs to die, if so kill it
void Chicken::haveToDie()
{
    if (hp <= 0) {
        drop_gifts();
        drop_food();
        dieSound();
        game->lm->waveSub();
        game->player_score->increseScore(game->player_score->chicken_points);
        destroy();
    }
}

// Filying animation
// changes images after wing_speed frames
void Chicken::FlapWings()
{
    flap_wing >= wing_speed ? flap_wing = 0 : flap_wing++;
    if (flap_wing < wing_speed / 2) {
        setPixmap(chicken_1);
    } else {
        setPixmap(chicken_2);
    }
}

// called with a timer each frame
void Chicken::advance(int a)
{
    (void)a;
    movement();
    shoot();
}

// move chicken in block formation
void Chicken::move_block()
{
    if (!pause) {
        if (!game->lm->move_direction) {
            if (pos().x() + speed_movement < game->game_width - 100) {
                setPos(x() + speed_movement, y());
            } else {
                game->lm->move_direction = true;
            }
        } else {
            if (pos().x() - speed_movement > 10) {
                setPos(x() - speed_movement, y());
            } else {
                game->lm->move_direction = false;
            }
        }
    }
}

// moves chicken straight down for 2*regualr movement speed
// if it leave the screen deastroy it
void Chicken::move_fall()
{
    if (!pause) {
        setPos(x(), y() + 2 + speed_movement);
        if (y() > game->game_height) {
            game->lm->waveSub();
            destroy();
        }
    }
}

// moves chicken straight across screen, direction depens on lvl
// lvl=5 => from left to right; lvl=8 => from right to left
// if it leave the screen deastroy it
void Chicken::move_left_right()
{
    if (!pause) {
        int movement_buffer = 127;
        if (game->lm->level_counter % 10 == 5) {
            // lvl=5 => from left to right
            setPos(x() + speed_movement, y());
            if (pos().x() + speed_movement > game->game_width + movement_buffer) {
                game->lm->waveSub();
                destroy();
            }
        } else if (game->lm->level_counter % 10 == 8) {
            // lvl=8 => from right to left
            setPos(x() - speed_movement, y());
            if (pos().x() - speed_movement < -movement_buffer) {
                game->lm->waveSub();
                destroy();
            }
        }
    }
}
