#include "code/include/meteor.h"
#include "code/include/bullet.h"
#include "code/include/chicken.h"
#include "code/include/droppable_items.h"
#include "code/include/game.h"
#include "code/include/level_manager.h"

extern Game* game;
extern bool pause;
extern Menu* menu;

Meteor::Meteor(int x, int y)
    : QObject()
    , QGraphicsPixmapItem()
{
    move_x = 0;
    move_y = 0;

    int meteorType = (rand() % 3) + 1;
    switch (meteorType) {
    case 1:
        setPixmap(Meteor_1);
        break;
    case 2:
        setPixmap(Meteor_2);
        break;
    case 3:
        setPixmap(Meteor_3);
        break;
    }

    int hp_level_interval = 20;
    hp = 2 * meteorType + game->lm->level_counter / hp_level_interval;

    setPos(x, y);

    int z = rand() % 3;
    move_x = z;
    move_y = z;

    // lvl=3 => standard meteor movement; lvl=7 => haotic movement
    if (game->lm->level_counter % 10 == 7) {
        int haotic_moving_parametar = rand() % 4;
        switch (haotic_moving_parametar) {
        case 1:
            move_y = 0;
            break;
        case 2:
            move_x = 0;
            break;
        default:
            move_y = rand() % 3;
        }
    }

    speed_movement = 1;
}

void Meteor::destroy()
{
    scene()->removeItem(this);
    game->cleaner->cleaner->append(this);
}

// drops a gift with a cirtain probability of 6%
// it is called only when a Meteor is destroyed
void Meteor::drop_gifts()
{
    int randShoot = (std::rand() % 100);
    if (randShoot <= 4) {
        Gift* gift = new Gift(x(), y());
        gift->setPos(x(), y() + 20);
        scene()->addItem(gift);
    }
}

// Decrease Meteor HP
void Meteor::decreaseHp(int x)
{
    hp -= x;
    haveToDie();
}

// check if the Meteor needs to die
void Meteor::haveToDie()
{
    if (hp <= 0) {
        drop_gifts();
        dieSound();
        game->lm->waveSub();
        game->player_score->increseScore(game->player_score->meteor_points);
        destroy();
    }
}

// makes sound when meteor is destroyed
void Meteor::dieSound()
{
    if (game->meteorSound->state() == QMediaPlayer::PlayingState) {
        game->meteorSound->setPosition(0);
    }
    game->meteorSound->play();
}

// function connect to timer
void Meteor::advance(int a)
{
    (void)a;
    move_fall();
}

// Meteor movement
void Meteor::move_fall()
{
    if (!pause) {
        setPos(x() + speed_movement + move_x, y() + speed_movement + move_y);

        if ((y() > game->game_height) || (x() > game->game_width)) {
            game->lm->waveSub();
            destroy();
        }
    }
}
