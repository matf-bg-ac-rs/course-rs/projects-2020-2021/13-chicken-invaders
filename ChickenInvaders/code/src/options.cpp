#include "code/include/options.h"
#include "code/include/game.h"
#include "code/include/menu.h"
#include "ui_options.h"

extern QMediaPlayer* music_player;
extern Menu* menu;

options::options(QWidget* parent)
    : QWidget(parent)
    , ui(new Ui::options)
{
    ui->setupUi(this);

    ui->back->move(menu->menu_width / 2 - ui->back->width() / 2 + 10 - 2 * ui->back->width() / 3,
        menu->menu_height / 2 + 5 * ui->back->height() / 2);
    ui->save->move(menu->menu_width / 2 - ui->back->width() / 2 + 10 + 2 * ui->back->width() / 3,
        menu->menu_height / 2 + 5 * ui->back->height() / 2);

    ui->volumeSlider->move(menu->menu_width / 2,
        menu->menu_height / 2 - ui->volume->height() - ui->volumeSlider->height() / 2);
    ui->muteCB->move(menu->menu_width / 2,
        menu->menu_height / 2 - ui->muteCB->height() / 2);

    ui->volume->move(menu->menu_width / 2 - 2 * ui->volume->width() / 2,
        menu->menu_height / 2 - 3 * ui->volume->height() / 2);
    ui->mute->move(menu->menu_width / 2 - 2 * ui->volume->width() / 2,
        menu->menu_height / 2 - ui->mute->height() / 2);

    if (menu->muted) {
        ui->muteCB->setChecked(true);
        ui->volumeSlider->setValue(0);
    } else {
        ui->volumeSlider->setValue(menu->volume);
    }
}

options::~options() { delete ui; }

// save changes
void options::on_save_clicked()
{
    menu->muted = ui->muteCB->isChecked();
    if (menu->muted) {
        ui->volumeSlider->setValue(0);
    }

    menu->volume = ui->volumeSlider->value();
    music_player->setVolume(menu->volume);
}

// go back
void options::on_back_clicked()
{
    menu->volume = ui->volumeSlider->value();
    music_player->setVolume(menu->volume);
    menu->muted = ui->muteCB->isChecked();
    music_player->setMuted(menu->muted);
    menu->showMaximized();
    this->close();
}

void options::paintEvent(QPaintEvent*)
{
    QStyleOption opt;
    opt.init(this);
    QPainter p(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
}
