#include "code/include/player_hud.h"
#include "code/include/game.h"
#include "code/include/meteor.h"
#include "code/include/player.h"

extern Game* game;

PlayerScore::PlayerScore(QGraphicsItem* parent)
    : QGraphicsTextItem(parent)
{
    score = 0;
    food_eaten = 0;
    new_life_score = 10000;
    drawText();
    chicken_points = 10;
    chicken_points_death = 50;
    meteor_points = 20;
    boss_points = 50;
    boss_points_death = 1000;
    setDefaultTextColor(Qt::yellow);
    setFont(QFont("times", 20));
}

// draw hud on screen
void PlayerScore::drawText()
{
    setPlainText(QString("Score: ") + QString::number(score) + QString("\n Food: ") + QString::number(food_eaten) + QString("\nLevel: ") + QString::number(level));
}

// increase score and gives life
void PlayerScore::increseScore(int increse)
{
    score += increse;
    drawText();

    // points needed to get a new life increse exponentionaly
    if (score >= new_life_score) {
        game->player->addPlayerHealth();
        new_life_score *= 2;
    }
}

// getter for score
int PlayerScore::getScore() { return score; }

// reset value for new game
void PlayerScore::resetScore()
{
    score = 0;
    food_eaten = 0;
    new_life_score = 10000;
    chicken_points = 10;
    chicken_points_death = 50;
    meteor_points = 20;
    boss_points = 50;
    boss_points_death = 1000;
    level = 1;
    drawText();
}

// updating score in later stages of game
void PlayerScore::setPointsOnLevelUp()
{
    level = game->lm->level_counter;
    const int level_interval = 10;
    chicken_points = 10 + 5 * (level / level_interval);
    chicken_points_death = 50 + 20 * (level / level_interval);
    meteor_points = 20 + 10 * (level / (2 * level_interval));
    boss_points = 50 + 15 * (level / level_interval);
    boss_points_death = 1000 + 20 * 25 * (level / level_interval);
    drawText();
}

// keep count of picked up food
void PlayerScore::addFoodEaten(int n) { food_eaten += n; }

PlayerHealth::PlayerHealth() { setPixmap(QPixmap(":/images/hp")); }

RocketCountIndikator::RocketCountIndikator()
{
    setPixmap(QPixmap(":/images/reinder"));
}
