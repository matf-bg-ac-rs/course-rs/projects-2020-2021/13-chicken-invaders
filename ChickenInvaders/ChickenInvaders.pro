QT       += core gui \
            multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11 \
          resources_big \
          debug

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    code/src/boss.cpp \
    code/src/bullet.cpp \
    code/src/chicken.cpp \
    code/src/cleaner.cpp \
    code/src/droppable_items.cpp \
    code/src/game.cpp \
    code/src/instructions.cpp \
    code/src/main.cpp \
    code/src/menu.cpp \
    code/src/meteor.cpp \
    code/src/options.cpp \
    code/src/player.cpp \
    code/src/player_hud.cpp \
    code/src/rocket.cpp \
    code/src/score.cpp \
    code/src/level_manager.cpp \
    code/src/hall_of_fame.cpp \
    code/src/pause.cpp \
    code/src/continue.cpp \
    code/src/credits.cpp \
    code/test/test.cpp \
    code/test/test_droppable_items.cpp \
    code/test/test_menu.cpp \
    code/test/test_rocket.cpp

HEADERS += \
    code/include/boss.h \
    code/include/bullet.h \
    code/include/catch.hpp \
    code/include/chicken.h \
    code/include/cleaner.h \
    code/include/droppable_items.h \
    code/include/game.h \
    code/include/instructions.h \
    code/include/menu.h \
    code/include/meteor.h \
    code/include/options.h \
    code/include/player.h \
    code/include/player_hud.h \
    code/include/rocket.h \
    code/include/score.h \
    code/include/level_manager.h \
    code/include/hall_of_fame.h \
    code/include/pause.h \
    code/include/continue.h \
    code/include/credits.h

FORMS += \
    code/res/instructions.ui \
    code/res/menu.ui \
    code/res/options.ui \
    code/res/score.ui \
    code/res/hall_of_fame.ui \
    code/res/pause.ui \
    code/res/continue.ui \
    code/res/credits.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources/ResourceFile.qrc

DISTFILES += \
    resources/images/candy
