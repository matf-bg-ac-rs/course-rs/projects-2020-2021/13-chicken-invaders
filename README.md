[![c_plus_plus](https://img.shields.io/badge/Language-C%2B%2B-blue)](https://www.cplusplus.com/)
[![qt5](https://img.shields.io/badge/Framework-Qt5-green)](https://doc.qt.io/qt-5/)
[![qtCreator](https://img.shields.io/badge/IDE-Qt_Creator-green)](https://www.qt.io/product/development-tools)


# Project 13-Chicken-Invaders

Ovo je igrica u kojoj igrac kontrolise mali svemirski brod putem strelica ili ASDW dugmica, ispaljuje projektile pritiskom SPACE dugmeta, i ispaljuje rakete preko SHIFT ili ENTER dugmica na tastaturi. Igrac prelazi nivoe tako sto treba da ubije ili izbegne sve nerpijatelje koje se pojave dok izbegava jaja koja pri kontaktu sa svemirskim brodom ekspolodiraju i oduzimaju igracu zivot. Neprijatelji mogu biti kokoske, kolacici ili velike sef kokoske. Igrac pocetno ima 3 zivota s tim da je u mogucnosti da dobije nove zivote tako sto sakuplja bodove, gde prvi dodatni zivot se dobija na 10000 bodova, a svaki sledeci zahteva duplo vise bodova nego prethodni. Skupljanjem paketica koji imaju odredjenu verovatnocu da padnu kada se ubiju neprijatleji igracu se ojacava oruzije. Oruzija imaju 13 nivoa, a neki nivoi zahtevaju vise od jednog paketica da se skupi da bi se dobilo unapredjenje. Pored zivota smrt igraca oduzima 3 nivoa oruzija, mada na jacim oruzijima zavisno od toga koliko je paketica skupljeno mogu da oduzmu jedan ili neijedan nivo. Posle smrti igrac dobija zastitu od smrti na 3 sekunde. Jacina zvuka u igrici se moze podesavati u glavnom meniju i u meniju za pauzu. Igra se zavrsava kada igrac predje svih 100 nivoa ili kada igrac izgubi sve zivote. Rezultat se onda upisuje u "Hall of Fame" ako je igrac ostvario dovoljno bodova da udje u top 10. Svaka kokoska kad umre ispusta peceni batak ili peceno pile (koje ima vrlo malu verovatnocu, i vredi kao 10 bataka). Na svakih 150 skupljenih bataka se dobija nova raketa.

# Demo 
- [demo-video](https://youtu.be/TYnxAINlQuM); [backup](https://youtu.be/shD4FEqEcu0) 

 
[![demo](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/13-chicken-invaders/-/raw/master/ChickenInvaders/resources/images/thumbnail03.jpeg)](https://youtu.be/TYnxAINlQuM)

# [Build](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/13-chicken-invaders/-/wikis/Prevo%C4%91enje)


# [Instruction](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/13-chicken-invaders/-/wikis/Tutorijal)


## Developers

- [Nikola Veselinović, 200/2015](https://gitlab.com/wesely1996)
- [Tamara Djukić, 242/2017](https://gitlab.com/TamaraDjukic)
- [Stefan Jaćović, 503/2017](https://gitlab.com/StefanJ996)
- [Kristina Petrović, 110/2015](https://gitlab.com/kpetrovicc)
- [Bojan Stefanović, 22/2015](https://gitlab.com/mi15022)

