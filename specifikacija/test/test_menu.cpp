#include "../../ChickenInvaders/code/include/catch.hpp"
#include "../../ChickenInvaders/code/include/menu.h"
#include "../../ChickenInvaders/code/include/score.h"
#include <QMediaPlayer>

#include <iostream>

TEST_CASE("Menu class"){
    Menu* menu = nullptr;

    SECTION("Create new menu test"){
        menu = new Menu();

        REQUIRE(menu != nullptr);
        REQUIRE(menu->volume == 0);

        delete menu;
    }

    SECTION("Test showing of score WIN"){
        menu = new Menu();
        menu->show_score(1);
        score* s = menu->findChild<score*>();
        REQUIRE(s != nullptr);
        REQUIRE(s->windowTitle() == "Congratulations! You saved Christmas!");

        s->close();

        delete menu;
    }

    SECTION("Test showing of score LOSE"){
        menu = new Menu();
        menu->show_score(0);
        score* s = menu->findChild<score*>();
        REQUIRE(s != nullptr);
        REQUIRE(s->windowTitle() == "Game lost! Good luck next time!");

        s->close();

        delete menu;
    }

}
