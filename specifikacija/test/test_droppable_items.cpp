#include "../../ChickenInvaders/code/include/catch.hpp"
#include "../../ChickenInvaders/code/include/droppable_items.h"
#include "../../ChickenInvaders/code/include/game.h"

Game* game = nullptr;

TEST_CASE("Egg class"){
    game = new Game();

    SECTION("Create Egg Test"){
        int x=1, y=2;
        Egg* egg = new Egg(x,y);

        REQUIRE(egg != nullptr);
        delete egg;
    }

    SECTION("Test egg position"){
        int x=1, y=2;
        Egg* egg = new Egg(x,y);

        REQUIRE(egg->x() == x && egg->y() == y);
        delete egg;
    }

    delete game;
}

TEST_CASE("Gift class"){
    game = new Game();

    SECTION("Create Gift Test"){
        int x=1, y=2;
        Gift* gift = new Gift(x,y);

        REQUIRE(gift != nullptr);
        delete gift;
    }

    SECTION("Test egg position"){
        int x=1, y=2;
        Gift* gift = new Gift(x,y);

        REQUIRE(gift->x() == x && gift->y() == y);
        delete gift;
    }

    delete game;
}
